@echo off
title DataServer

set NODE_ENV=production
set MODULE=DataServer
set BIND_PORT=8073

node --no-warnings bin/index