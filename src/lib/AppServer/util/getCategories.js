import md5 from "md5";
import crud from "@lib/DataServer/CRUD";
import WebMallCategory from "@models/WebMallCategory";

async function getCategories() {
  try {
    const category_model = new crud(WebMallCategory);

    const {results, total} = await category_model.read({
      eager: {
        subCategories: {},
      },
      order: "shop_order asc",
    });

    // console.log({results})

    return {results, total};
  } catch (e) {
    console.log(e);
    return {};
  }
}

export default getCategories;
