import md5 from "md5";
import crud from "@lib/DataServer/CRUD";
import WebPackageItem from "@models/WebPackageItem";

async function getCategoryItems(shop_no, shop_no_sub) {
  try {
    if (!shop_no || !shop_no_sub) throw "INVALID_ARGUMENTS";
    const model = new crud(WebPackageItem);

    const {results, total} = await model.read({
      eager: {
        $where: {
          service: 1,
          shop_no,
          shop_no_sub,
        },
        // includes:
        detail: {
          $where: {
            package_id: {
              $exists: true,
            },
          },
        },
        lang: {},
        mall: {
          $where: {
            active: 1,
          },
        },
        preview: {},
      },
      order: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.item_order asc",
    });

    // console.log({results})

    return {results, total};
  } catch (e) {
    console.log(e);
    return {};
  }
}

export default getCategoryItems;
