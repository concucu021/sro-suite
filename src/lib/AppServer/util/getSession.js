import md5 from "md5";
import crud from "@lib/DataServer/CRUD";
import User from "@lib/DataServer/models/User";

// This is authentication:
async function getSession(jid, key) {
  if (!jid || !key) throw "INVALID_PAYLOAD";

  const user_model = new crud(User);

  const {
    results: [user],
  } = await user_model.read({
    eager: {
      $where: {
        JID: jid,
      },
      wallet: {},
      mall_token: {},
    },
  });

  const {
    mall_token: {Certifykey},
  } = user;

  const _key = md5(`${jid}${Certifykey}eset5ag.nsy-g6ky5.mp`).toUpperCase();

  // uncomment for verification:
  if (_key !== key) throw new Error("NOT_AUTHORIZED");

  return user;
}

export default getSession;
