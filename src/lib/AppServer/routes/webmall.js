import getSession from "../util/getSession";
import getCategories from "../util/getCategories";
import getCategoryItems from "../util/getCategoryItems";

// webapp:
const index = async (req, res) => {
  try {
    // get the params from queryString:
    const {jid, key, loc} = req.query;

    const sessionPayload = btoa([jid, key, loc].join("|"));

    // Retrieve & validate session from token/jid:
    const session = await getSession(jid, key);

    // expose session data to the template layer:
    return res.view("webmall/index", {
      session: {
        token: sessionPayload,
        ...session,
      },
    });
  } catch (e) {
    return res.view("default");
  }
};

// fetch category items
const api_items = async (req, res) => {
  try {
    const {shop_no, shop_no_sub} = req.query;

    const items = await getCategoryItems(shop_no, shop_no_sub);

    return items;
  } catch (e) {
    res.view("default");
  }
};

// fetch categories + sub categories tree
const api_categories = async (req, res) => {
  try {
    const categories = await getCategories();

    return categories;
  } catch (e) {
    res.view("default");
  }
};

// fetch session update
const api_query_update = async (req, res) => {
  try {
    const {jid, key} = req.query;
    const session = await getSession(jid, key);

    return session;
  } catch (e) {
    return res.view("default");
  }
};

export default {
  index,
  api_categories,
  api_query_update,
  api_items,
};
