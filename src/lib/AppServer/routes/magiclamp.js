import getSession from "../util/getSession";
import getCategories from "../util/getCategories";
import getCategoryItems from "../util/getCategoryItems";

// webapp:
const index = async (req, res) => {
  try {
    // get the params from queryString:
    const {jid, key, loc} = req.query;

    const sessionPayload = btoa([jid, key, loc].join("|"));

    // Retrieve & validate session from token/jid:
    const session = await getSession(jid, key);

    // expose session data to the template layer:
    return res.view("magiclamp/index", {
      session: {
        token: sessionPayload,
        ...session,
      },
    });
  } catch (e) {
    return res.view("default");
  }
};


export default {
  index,
};
