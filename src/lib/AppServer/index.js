import fastify from "fastify";
import {Model} from "objection";
import knex from "knex";
import dbConfig from "../DataServer/config";
import models from "../DataServer/models";
import CRUD from "../DataServer/CRUD";
import {camelCase, startCase} from "lodash";
import oas from "fastify-oas";
import path from "path";
import viewEngine from "point-of-view";
import staticFilesEngine from "@fastify/static";
import handlebars from "handlebars";

class WebAppServer_Error extends Error {
  constructor(message = "UNKNOWN_ERROR") {
    super();
    this.message = message;
    this.statusCode = 404;
  }
}

class WebAppServer {
  constructor(config) {
    this.config = config;
    this.server = fastify(this.config);

    this.database = knex(dbConfig);
    Model.knex(this.database);

    this.server.register(viewEngine, {
      root: path.join(__dirname, "views"),
      engine: {
        handlebars,
      },
    });

    this.server.register(staticFilesEngine, {
      root: path.join(__dirname, "..", "..", "..", "public"),
    });
  }

  async run() {
    try {
      const {port, host} = this.config;
      this.server.setNotFoundHandler(function (req, res) {
        res.view("default");
      });
      this.server.listen(port, host);
      this.server.ready(error => {
        if (error) throw new WebAppServer_Error(error.message);
        console.log(`[ready] ${host}:${port}`);
      });
    } catch (error) {
      this.server.log.error(error);
      process.exit(1);
    }
  }
}

export default WebAppServer;
