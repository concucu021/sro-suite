import Security from "@lib/Security/Protocol";
import {Reader, Writer} from "./Packet";
import {translate} from "@lib/workers/Client/helpers";
import NodeCache from "node-cache";
import Instance from "./Instance";

const [
  NONE,
  BLOWFISH,
  SECURITY_BYTES,
  HANDSHAKE,
  HANDSHAKE_RESPONSE,
] = [
  "none",
  "blowfish",
  "security_bytes",
  "handshake",
  "handshake_response",
];

class DataStream_Error extends Error {
  constructor(message, data) {
    super(message);
    this.data = data;
  }
}

class DataStream {
  constructor() {
    this.security = new Security("SR_Client");
    this.instances = [];

    this.direction = {
      SERVER: "server",
      CLIENT: "client",
    };

    this.reader = Reader;
    this.writer = Writer;

    this._stored = {
      buffer: [],
      length: 0,
    };
  }

  // Create an instance for each client connected, but store only the buffer results:
  createInstance(id) {
    this.instances[id] = new Instance();

    this.instances[id].initial_security = this.security.generate([BLOWFISH, SECURITY_BYTES, HANDSHAKE]);
  }

  parse(id, data) {
    var pointer = 0;
    var tmpLength = data.length;
    var buffers = [];
    var totalBuffer;

    if (this._stored?.buffer?.length > 0) {
      totalBuffer = new Buffer(this._stored.buffer.length + data.length);
      this._stored.buffer.copy(totalBuffer);
      data.copy(totalBuffer, this._stored.buffer.length);
      tmpLength = this._stored.buffer.length + data.length;
      this._stored.length = 0;
    }

    while (tmpLength > 0) {
      var encrypted = false;
      var realSize = 0;
      var packetSize = (data[pointer + 1] << 8) | data[pointer];
      if ((packetSize & 0x8000) > 0) {
        packetSize &= 0x7fff;
        realSize = packetSize;
        encrypted = true;
        packetSize = 2 + this.security.getOutputLength(packetSize + 4);
      } else {
        packetSize += 6;
      }

      if (packetSize > tmpLength) {
        this._stored.buffer = new Buffer(tmpLength);
        data.copy(this._stored.buffer, 0, pointer);
        this._stored.buffer.length = tmpLength;
        tmpLength = 0;
      } else {
        var currentBuffer = new Buffer(packetSize);
        data.copy(currentBuffer, 0, pointer, pointer + packetSize);
        var tmp = {
          size: packetSize,
          buffer: currentBuffer,
          encry: encrypted,
          real: realSize,
        };
        buffers.push(tmp);
        pointer += packetSize;
        tmpLength -= packetSize;
      }
    }

    if (buffers.length) return this.unpack(buffers);
  }

  unpack(buffers) {
    for (const {encrypted, buffer, size, real: realSize} of buffers) {
      var packet;
      if (encrypted) {
        var decrypted = this.security.decode(buffer, 2, size - 2);
        var real = new Buffer(size + 2);
        real.writeUInt16LE(realSize, 0);
        decrypted.copy(real, 2);
        packet = new Reader(real);
        packet.encrypted = true;
      } else {
        packet = new Reader(buffer);
        packet.encrypted = false;
      }

      packet.t_opcode = translate(packet.opcode);

      try {
        return {direction: this.direction.SERVER, packet};
      } catch (e) {
        throw new DataStream_Error("FAILED_TO_PROCESS", {
          encrypted,
          buffer,
          size,
          realSize,
          error: e,
        });
      }
    }
  }
}

export const SECURITY_FLAGS = {
  NONE,
  BLOWFISH,
  SECURITY_BYTES,
  HANDSHAKE,
  HANDSHAKE_RESPONSE,
  // not giving a fuck about the rest 3
};

export default DataStream;
