class ByteUtils_Error extends Error {
  constructor(message, input, stack) {
    super(message);
    this.data = input;
    this.stack = stack;
  }
}

export const UINT32 = value => {
  var buffer = new Buffer(4);
  buffer.writeInt32LE(value, 0);

  return buffer.readUInt32LE(0);
};

export const LONGLONG = (a, b) => {
  try {
    const buffer = new ArrayBuffer(16);
    const value = (b << 32) | a;
    const view = new DataView(buffer);

    view.setBigUint64(0, value, true);
    return view.getBigUint64(0, true);
  } catch (e) {
    throw new ByteUtils_Error("LONGLONG conversion failed", [0, value, true], e);
  }
};

export const LONG = (a, b) => {
  var buffer = new Buffer(4);
  var value = (b << 16) | a;

  buffer.writeInt32LE(value, 0);
  return buffer.readUInt32LE(0);
};

export const WORD = (a, b) => {
  var buffer = new Buffer(2);
  var value = (b << 8) | a;

  buffer.writeInt16LE(value, 0);
  return buffer.readUInt16LE(0);
};

export const LOWORD = a => {
  var buffer = new Buffer(2);
  var value = a & 0xffff;

  buffer.writeUInt16LE(value, 0);
  return buffer.readUInt16LE(0);
};

export const HIWORD = a => {
  return (a >>> 16) & 0xffff;
};

export const LOBYTE = a => {
  return a & 0xff;
};

export const HIBYTE = a => {
  return (a >>> 8) & 0xff;
};
