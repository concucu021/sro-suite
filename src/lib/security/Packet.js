class Packet {
  constructor() {
    this.pointer = 0;
    this.size = 0;
    this.buffer = null;
  }

  setPointer(i) {
    this.pointer = i;
  }

  resetPointer() {
    this.pointer = this.size;
  }

  getBytes() {
    return this.buffer.slice(0, this.size);
  }

  toData() {
    return this.buffer.slice(0, this.size).toJSON().data;
  }
}

class Writer extends Packet {
  constructor(buffer) {
    super();

    this.buffer = Buffer.alloc(4096);

    if (buffer) {
      buffer.copy(this.buffer);
      this.pointer = this.size = buffer.length;
    }
  }

  uint8(b) {
    this.buffer.writeUInt8(b, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += 1;
      this.size += 1;
    } else {
      this.pointer += 1;
    }
  }

  uint16(w) {
    this.buffer.writeUInt16LE(w, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += 2;
      this.size += 2;
    } else {
      this.pointer += 2;
    }
  }

  uint32(dw) {
    this.buffer.writeUInt32LE(dw, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += 4;
      this.size += 4;
    } else {
      this.pointer += 4;
    }
  }

  double(qw) {
    this.buffer.writeDoubleLE(qw, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += 8;
      this.size += 8;
    } else {
      this.pointer += 8;
    }
  }

  string(str) {
    const test = new String(str);
    const len = test.length;
    this.writeWord(len);
    this.buffer.write(str, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += len;
      this.size += len;
    } else {
      this.pointer += len;
    }
  }

  float(f) {
    this.buffer.writeFloatLE(f, this.pointer);
    if (this.pointer == this.size) {
      this.pointer += 4;
      this.size += 4;
    } else {
      this.pointer += 4;
    }
  }
}

class Reader extends Packet {
  constructor(buffer) {
    super();

    this.buffer = Buffer.from(buffer);
    this.size = buffer.readUInt16LE(0);
    this.opcode = buffer.readUInt16LE(2);
    this.securityCount = buffer.readUInt8(4);
    this.securityCRC = buffer.readUInt8(5);
    this.pointer = 6;
    this.encrypted = false;
  }

  uint8() {
    const value = this.buffer.readUInt8(this.pointer);
    this.pointer++;
    return value;
  }

  uint16() {
    const value = this.buffer.readUInt16LE(this.pointer);
    this.pointer += 2;
    return value;
  }

  uint32() {
    const value = this.buffer.readUInt32LE(this.pointer);
    this.pointer += 4;
    return value;
  }

  uint64() {
    const value = this.buffer.readUIntLE(this.pointer, 6);
    this.pointer += 8;
    return value;
  }

  double() {
    const tmp = this.buffer.readDoubleLE(this.pointer);
    this.pointer += 8;
    return tmp;
  }

  string() {
    const len = this.uint16();
    const str = this.buffer.toString("utf8", this.pointer, this.pointer + len);
    this.pointer += len;
    return str;
  }

  float() {
    const value = this.buffer.readFloatLE(this.pointer);
    this.pointer += 4;
    return value;
  }

  bool() {
    if (this.buffer.readUInt8(this.pointer) === 1) {
      return true;
    } else {
      return false;
    }
  }

  uint8array(size) {
    return this.buffer.slice(this.pointer, this.pointer + size);
  }
}

export {Writer, Reader};
