import NodeCache from "node-cache";
import Blowfish from "./Blowfish";



const createBuffer = size => new DataView(new ArrayBuffer(size));

/**
 * This will be the default state of each client:
 */
const SESSION = () => ({
    initial_security: null,
    blowfish: new Blowfish(),
    protocol: {
        initial_blowfish_key: createBuffer(8),
        handshake_blowfish_key: createBuffer(8),
        client_key: createBuffer(8),
        key_array: createBuffer(8),
        seed_count: 0,
        crc_seed: 0,
        count_byte_seeds: createBuffer(3),
    },
    
});

class Instance {
    constructor() {
        Object.assign(this, SESSION());
    }

    setSecurity(buffer) {
        this.initial_security = buffer;
    }



}


export default Instance;