import Blowfish from "./Blowfish";
import {UINT32, LOBYTE, HIBYTE, LOWORD, HIWORD, LONGLONG} from "./ByteUtils";
import Long from "long";
import {Writer} from "./Packet";
import security_table from "./data/security_table";

const createConfig = (arr, defaultValue) =>
  arr.reduce(
    (obj, key) => ({
      ...obj,
      [key]: defaultValue,
    }),
    {},
  );

class Protocol_Error extends Error {
  constructor(message, stack) {
    super();
    this.message = message;
    this.data = stack || false;
  }
}

class Protocol {
  constructor(identity_name = "SR_Client") {
    this.m_initial_blowfish_key = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);
    this.m_seed_count = 0;
    this.m_crc_seed = 0;
    this.m_handshake_blowfish_key = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);
    this.m_value_g = 0;
    this.m_value_p = 0;
    this.m_value_A = 0;
    this.m_value_x = 0;
    this.m_value_B = 0;
    this.m_value_K = 0;
    this.m_count_byte_seeds = new Buffer([0, 0, 0]);
    this.m_client_key = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);
    this.m_challenge_key = 0;
    this.key_array = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);

    this.m_client_security = false;
    this.m_security_flag = 0;

    // this.m_security_flags = {
    //   none: 0,
    //   blowfish: 0,
    //   security_bytes: 0,
    //   handshake: 0,
    //   handshake_response: 0,
    //   _6: 0,
    //   _7: 0,
    //   _8: 0,
    // };

    this.m_accepted_handshake = false;
    this.m_started_handshake = false;

    this.m_blowfish = new Blowfish();

    this.m_identity_flag = 0;
    this.m_identity_name = "SR_Client";
  }

  generateValue(val) {
    for (var i = 0; i < 32; ++i) {
      val = (((((((((((val >>> 2) ^ val) >>> 2) ^ val) >>> 1) ^ val) >>> 1) ^ val) >>> 1) ^ val) & 1) | ((((val & 1) << 31) | (val >>> 1)) & 0xfffffffe);
    }

    return UINT32(val);
  }

  setupCountByte(seed) {
    if (seed == 0) seed = 0x9abfb3b6;
    var mut = seed;
    var mut1 = this.generateValue(mut);
    mut = mut1;
    var mut2 = this.generateValue(mut);
    mut = mut2;
    var mut3 = this.generateValue(mut);
    mut = mut3;
    mut = this.generateValue(mut);
    var byte1 = (mut & 0xff) ^ (mut3 & 0xff);
    var byte2 = (mut1 & 0xff) ^ (mut2 & 0xff);
    if (byte1 == 0) byte1 == 1;
    if (byte2 == 0) byte2 == 1;
    this.m_count_byte_seeds[0] = byte1 ^ byte2;
    this.m_count_byte_seeds[1] = byte2;
    this.m_count_byte_seeds[2] = byte1;
  }

  G_pow_X_mod_P(P, X, G) {
    var result = 1;
    var mult = G;
    if (X == 0) {
      return 1;
    }
    while (X != 0) {
      if ((X & 1) > 0) {
        result = (mult * result) % P;
      }
      X = X >>> 1;
      mult = (mult * mult) % P;
    }
    return result;
  }

  keyTransformValue(val1, val2, key, key_byte) {
    var stream = new Buffer(8);
    stream.writeUInt32LE(val1, 0);
    stream.writeUInt32LE(val2, 4);

    stream[0] ^= stream[0] + LOBYTE(LOWORD(key)) + key_byte;
    stream[1] ^= stream[1] + HIBYTE(LOWORD(key)) + key_byte;
    stream[2] ^= stream[2] + LOBYTE(HIWORD(key)) + key_byte;
    stream[3] ^= stream[3] + HIBYTE(HIWORD(key)) + key_byte;
    stream[4] ^= stream[4] + LOBYTE(LOWORD(key)) + key_byte;
    stream[5] ^= stream[5] + HIBYTE(LOWORD(key)) + key_byte;
    stream[6] ^= stream[6] + LOBYTE(HIWORD(key)) + key_byte;
    stream[7] ^= stream[7] + HIBYTE(HIWORD(key)) + key_byte;

    console.log({keyTransformValue: stream});

    return stream;
  }

  generateCountByte(update) {
    var result = LOBYTE(this.m_count_byte_seeds[2] * (~this.m_count_byte_seeds[0] + this.m_count_byte_seeds[1]));
    result = result ^ (result >>> 4);
    if (update) {
      this.m_count_byte_seeds[0] = result;
    }
    return result;
  }

  generateCheckByte(stream, offset, lenght) {
    var checksum = 0xffffffff;
    var lchecksum = Long.fromNumber(0xffffffff);
    var ff = Long.fromNumber(0xff);
    var moddedseed = this.m_crc_seed << 8;
    for (var x = offset; x < offset + lenght; ++x) {
      var tmp = Long.fromNumber(stream[x]);
      tmp = tmp.xor(lchecksum);
      tmp = tmp.and(ff);
      var index = moddedseed + tmp.toInt();
      lchecksum = lchecksum.shiftRight(8);
      var tablevalue = Long.fromNumber(security_table[index]);
      lchecksum = lchecksum.xor(tablevalue);
    }
    var tmp = lchecksum.shiftRightUnsigned(24);
    tmp = tmp.and(ff);
    var v1 = lchecksum.shiftRightUnsigned(8);
    v1 = v1.and(ff);
    var v2 = lchecksum.shiftRightUnsigned(16);
    v2 = v2.and(ff);
    var v3 = lchecksum.and(ff);
    tmp = tmp.add(v1);
    tmp = tmp.add(v2);
    tmp = tmp.add(v3);
    var test = tmp.toInt();
    var bit = LOBYTE(test);
    return bit;
  }

  decode(data, offset, length) {
    return this.m_blowfish.decode(data, offset, length);
  }

  getOutputLength(len) {
    return this.m_blowfish.getOutputLength(len);
  }

  fromSecurityFlags(flags) {
    return flags.none | (flags.blowfish << 1) | (flags.security_bytes << 2) | (flags.handshake << 3) | (flags.handshake_response << 4) | (0 << 5) | (0 << 6) | (0 << 7);
  }

  // ULTRA DUMB:
  handshake(packet, security_flags) {
    if (packet.encrypted) throw new Error("ENCRYPTED_PACKET");

    if (this.m_client_security) {
      if (m_security_flags.handshake == 0) {
        if (packet.opcode == 0x9000) {
          if (this.m_accepted_handshake) throw new Protocol_Error("DUPLICATE_0x9000");
          this.m_accepted_handshake = true;
          return;
        } else if (packet.opcode == 0x5000) throw new Protocol_Error("MISSING_HANDSHAKE_0x5000");
        else throw new Protocol_Error("INVALID_HANDSHAKE");
      } else {
        if (packet.opcode == 0x9000) {
          if (!this.m_started_handshake) throw new Protocol_Error("INVALID_ORDER_0x9000");
          if (this.m_accepted_handshake) throw new Protocol_Error("DUPLICATE_0x9000");

          this.m_accepted_handshake = true;
          return;
        } else if (packet.opcode == 0x5000) {
          if (this.m_started_handshake) throw new Protocol_Error("DUPLICATE_0x5000");
          this.m_started_handshake = true;
        } else throw new Protocol_Error("ILLOGICAL_HANDSHAKE");
      }

      this.m_value_B = packet.uint32();
      this.m_client_key = packet.uint64();

      this.m_value_K = this.G_pow_X_mod_P(this.m_value_p, this.m_value_x, this.m_value_B);

      this.key_array = this.keyTransformValue(this.m_value_A, this.m_value_B, this.m_value_K, LOBYTE(LOWORD(this.m_value_K)) & 0x03);

      this.m_blowfish.init(this.key_array);

      this.m_client_key = this.m_blowfish.decode(this.m_client_key, 0, 8);

      this.key_array = this.keyTransformValue(this.m_value_B, this.m_value_A, this.m_value_K, LOBYTE(LOWORD(this.m_value_B)) & 0x07);

      if (this.m_client_key.readDoubleLE(0) != this.key_array.readDoubleLE(0)) {
        throw new Protocol_Error("SIGNATURE_ERROR", {
          m_client_key: this.m_client_key.readDoubleLE(0),
          key_array: this.key_array.readDoubleLE(0),
        });
      }

      this.key_array = this.keyTransformValue(this.m_value_A, this.m_value_B, this.m_value_K, LOBYTE(LOWORD(this.m_value_K)) & 0x03);

      this.m_blowfish.init(this.key_array);

      this.m_challenge_key = this.keyTransformValue(this.m_value_A, this.m_value_B, this.m_value_K, LOBYTE(LOWORD(this.m_value_K)) & 0x07);

      this.m_challenge_key = this.m_blowfish.encode(this.m_challenge_key, 0, 8).buff;

      this.m_handshake_blowfish_key = this.keyTransformValue(this.m_handshake_blowfish_key.readUInt32LE(0), this.m_handshake_blowfish_key.readUInt32LE(4), this.m_value_K, 0x3);

      this.m_blowfish.init(this.m_handshake_blowfish_key);

      var tmp_flag,
        response,
        returnPackets = [];

      tmp_flag = this.fromSecurityFlags({
        ...m_security_flags,
        handshake_response: 1,
      });

      response = new Writer();
      response.uint8(tmp_flag);
      response.double(this.m_challenge_key.readDoubleLE(0));

      returnPackets.push({
        opcode: 0x5000,
        packet: response,
        encrypted: false,
      });

      return returnPackets;
    } else {
      if (packet.opcode != 0x5000) throw new Protocol_Error("ILLOGICAL_HANDSHAKE");

      var flag = packet.uint8(),
        flags = this.toSecurityFlags(flag);

      if (this.m_security_flag == 0) {
        this.m_security_flag = flag;
        m_security_flags = flags;
      }

      if (flags.blowfish == 1) {
        this.m_initial_blowfish_key.writeDoubleLE(packet.double(), 0);
        this.m_blowfish.init(this.m_initial_blowfish_key);
      }

      if (flags.security_bytes == 1) {
        this.m_seed_count = packet.uint32();
        this.m_crc_seed = packet.uint32();
        this.setupCountByte(this.m_seed_count);
      }

      if (flags.handshake == 1) {
        this.m_handshake_blowfish_key.writeDoubleLE(packet.double(), 0);
        this.m_value_g = packet.uint32();
        this.m_value_p = packet.uint32();
        this.m_value_A = packet.uint32();
        this.m_value_x = 0x33333333 & 0x7fffffff;
        this.m_value_B = this.G_pow_X_mod_P(this.m_value_p, this.m_value_x, this.m_value_g);
        this.m_value_K = this.G_pow_X_mod_P(this.m_value_p, this.m_value_x, this.m_value_A);
        this.key_array = this.keyTransformValue(this.m_value_A, this.m_value_B, this.m_value_K, LOBYTE(LOWORD(this.m_value_K)) & 0x03);
        this.m_client_key = this.keyTransformValue(this.m_value_B, this.m_value_A, this.m_value_K, LOBYTE(LOWORD(this.m_value_B)) & 0x07);
        this.m_blowfish.init(this.key_array);
        this.m_client_key = this.m_blowfish.encode(this.m_client_key, 8).buff;
      }

      if (flags.handshake_response == 1) {
        var expected_challenge_key;

        this.m_challenge_key = packet.double();
        expected_challenge_key = this.keyTransformValue(this.m_value_A, this.m_value_B, this.m_value_K, LOBYTE(LOWORD(this.m_value_A)) & 0x07);
        expected_challenge_key = this.m_blowfish.encode(expected_challenge_key, 8).buff;
        expected_challenge_key = expected_challenge_key.readDoubleLE(0);

        if (this.m_challenge_key != expected_challenge_key) throw new Protocol_Error("INVALID_CLIENT_SIGNATURE");

        this.m_handshake_blowfish_key = this.keyTransformValue(this.m_handshake_blowfish_key.readUInt32LE(0), this.m_handshake_blowfish_key.readUInt32LE(4), this.m_value_K, 0x3);
        this.m_blowfish.init(this.m_handshake_blowfish_key);
      }

      if (flags.handshake == 1 && flags.handshake_response == 0) {
        var returnPackets = [];
        var response;

        if (this.m_started_handshake || this.m_accepted_handshake) throw new Protocol_Error("ILLOGICAL_HANDSHAKE_PACKET");

        response = new Writer();
        response.uint32(this.m_value_B);
        response.double(this.m_client_key.readDoubleLE(0));

        returnPackets.push({
          opcode: 0x5000,
          packet: response,
          encrypted: false,
        });

        this.m_started_handshake = true;

        return returnPackets;
      } else {
        var returnPackets = [];
        var response1;
        var response2;

        if (this.m_accepted_handshake) throw new Protocol_Error("ILLOGICAL_HANDSHAKE_PACKET");

        //Handshake accept
        response1 = new Writer();
        returnPackets.push({
          opcode: 0x9000,
          packet: response1,
          encrypted: false,
        });

        // Identify
        response2 = new Writer();
        response2.string(this.m_identity_name);
        response2.uint8(this.m_identity_flag);

        returnPackets.push({
          opcode: 0x2001,
          packet: response2,
          encrypted: true,
        });

        // Mark the handshake as accepted now
        this.m_started_handshake = true;
        this.m_accepted_handshake = true;
        return returnPackets;
      }
    }
  }

  formatPacket(opcode, data, encrypted, m_security_flags) {
    var dataLength, packet, countByte, checkByte;
    dataLength = data.length;

    if (dataLength >= 0x8000) throw new Protocol_Error("OVERSIZED_PAYLOAD");
    packet = new Buffer(dataLength + 6);

    // Add the packet header to the start of the data
    packet.writeUInt16LE(dataLength, 0);
    packet.writeUInt16LE(opcode, 2);
    packet.writeUInt16LE(0, 4);

    if (dataLength) {
      data.copy(packet, 6);
    }

    // Determine if we need to mark the packet size as encrypted
    if (encrypted && (m_security_flags.blowfish == 1 || (m_security_flags.security_bytes == 1 && m_security_flags.blowfish == 0))) {
      packet.writeUInt16LE(dataLength | 0x8000, 0);
    }

    // Only need to stamp bytes if this is a clientless object
    if (this.m_client_security == false && m_security_flags.security_bytes == 1) {
      countByte = this.generateCountByte(true);
      packet.writeUInt8(countByte, 4);
      checkByte = this.generateCheckByte(packet, 0, packet.length);
      packet.writeUInt8(checkByte, 5);
    }
    // If the packet should be physically encrypted, return an encrypted version of it
    if (encrypted && m_security_flags.blowfish == 1) {
      var encryptedData, realPacket;
      encryptedData = this.m_blowfish.encode(packet, dataLength + 4).buff;
      packet = new Buffer(encryptedData.length + 2);
      packet.writeUInt16LE(dataLength | 0x8000, 0);
      encryptedData.copy(packet, 2);
    } else {
      packet.writeUInt16LE(dataLength, 0);
    }
    return packet;
  }

  toSecurityFlags(value) {
    flags = new Object();
    flags.none = value & 1;
    value >>= 1;
    flags.blowfish = value & 1;
    value >>= 1;
    flags.security_bytes = value & 1;
    value >>= 1;
    flags.handshake = value & 1;
    value >>= 1;
    flags.handshake_response = value & 1;
    value >>= 1;
    flags._6 = value & 1;
    value >>= 1;
    flags._7 = value & 1;
    value >>= 1;
    flags._8 = value & 1;
    value >>= 1;
    return flags;
  }

  generate(flags) {
    // Generate security
    var response;
    this.m_security_flag = this.fromSecurityFlags(flags);

    this.m_client_security = true;

    response = new Writer();
    response.uint8(this.m_security_flag);

    if (flags.blowfish == 1) {
      this.m_initial_blowfish_key.writeDoubleLE(0xdeadbeefcafebabe, 0);
      response.double(0xdeadbeefcafebabe);
    }

    if (flags.security_bytes == 1) {
      this.m_seed_count = 0x9abfb3b6;
      this.setupCountByte(this.m_seed_count);
      this.m_crc_seed = 0x9abfb3b6;

      response.uint32(this.m_seed_count);
      response.uint32(this.m_crc_seed);
    }

    if (flags.handshake == 1) {
      this.m_handshake_blowfish_key.writeDoubleLE(0xdeadbeefcafebabe, 0);
      this.m_value_x = 0x9abfb3c6 & 0x7fffffff;
      this.m_value_g = 0x9abfb3d6 & 0x7fffffff;
      this.m_value_p = 0x9abfb3e6 & 0x7fffffff;
      this.m_value_A = this.G_pow_X_mod_P(this.m_value_p, this.m_value_x, this.m_value_g);

      response.double(0xdeadbeefcafebabe);
      response.uint32(this.m_value_g);
      response.uint32(this.m_value_p);
      response.uint32(this.m_value_A);
    }

    return response.toData();
  }
}

export default Protocol;
