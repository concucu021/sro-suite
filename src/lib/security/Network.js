import DataStream from "./DataStream";
import net from "net";
import moment from "moment";
import {EventEmitter} from "events";
import * as ctrl from "@control";

/**
 * Manages clients and their connections between proxy -> server
 */
const DEFAULT_NETWORK_STATE = {
  stream: new DataStream(),
  remotes: {},
};

class Network extends EventEmitter {
  constructor(config) {
    super();
    Object.assign(this, {...DEFAULT_NETWORK_STATE, config});
    this.initMiddlewares();
  }

  initMiddlewares() {
    this.middlewares = Object.keys(this.config.middlewares).reduce(
      (endpoints, key) => ({
        ...endpoints,
        [key]: Object.keys(this.config.middlewares[key]).reduce(
          (handles, opcode) => ({
            ...handles,
            [opcode]: ctrl[this.config.middlewares[key][opcode]],
          }),
          {},
        ),
      }),
      {},
    );
  }

  assignUniqueInstance(input) {
    const instanceId = Buffer.from(input, "utf8").toString("base64");
    this.stream.createInstance(instanceId)
    return instanceId;
  }

  // Parses client -> server packets, applies interception based on requirement at @config/SERVER_MODULE_NAME
  async send(id, buffer) {
    // const {direction, packet} = this.stream.parse(id, buffer);
    // const operation = packet.opcode.toString();
    // const middleware = this.middlewares[direction][operation] || false;

    // if (middleware) {
    //   const middleware_result = await middleware(packet, this.stream, direction);
    // }

    // // console.log({operation, direction, packet});

    // Network -> Server
    this.remotes[id].server.write(buffer);
  }

  addConnection(id) {
    this.remotes[id] = net.connect(this.config.REMOTE)
    // Server -> Network
    this.remotes[id].server.on("data", buffer =>
      this.emit("data", {
        id,
        buffer,
      }),
    );

    ["error", "close"].map(code => this.remotes[id].on(code, () => console.log({id, code})));

    return this.remotes[id];
  }
}

export default Network;
