import {UINT32} from "./ByteUtils";
import {PArray, SBoxes, bf_P, bf_S} from "./data/blowfish";

class Blowfish_Error extends Error {
  constructor(message, stack) {
    super();
    this.message = message;
    this.stack = stack || false;
  }
}

class Blowfish {
  constructor(key) {
    Object.assign(this, {
      PArray,
      SBoxes,
      bf_P,
      bf_S,
    });

    if (key) this.init(key);
  }

  init(key) {
    let i, j;
    let data, datal, datar;

    for (i = 0; i < 18; ++i) {
      this.PArray[i] = this.bf_P[i];
    }

    for (i = 0; i < 4; i++) {
      for (j = 0; j < 256; j++) {
        this.SBoxes[i][j] = this.bf_S[i][j];
      }
    }

    let temp = new Buffer(4);

    j = 0;

    for (i = 0; i < 16 + 2; ++i) {
      temp[3] = key[j];
      temp[2] = key[(j + 1) % 8];
      temp[1] = key[(j + 2) % 8];
      temp[0] = key[(j + 3) % 8];

      data = temp.readUInt32LE(0, true);

      this.PArray[i] ^= data;
      this.PArray[i] = UINT32(this.PArray[i]);

      j = (j + 4) % 8;
    }

    datal = 0;
    datar = 0;

    for (i = 0; i < 16 + 2; i += 2) {
      let {xl, xr} = this.encipher(datal, datar);
      datal = xl;
      datar = xr;
      this.PArray[i] = datal;
      this.PArray[i + 1] = datar;
    }

    for (i = 0; i < 4; ++i) {
      for (j = 0; j < 256; j += 2) {
        let {xl, xr} = this.encipher(datal, datar);
        datal = xl;
        datar = xr;
        this.SBoxes[i][j] = xl;
        this.SBoxes[i][j + 1] = xr;
      }
    }
  }

  s(x, i) {
    if (i < 0 || i > 3) throw new Blowfish_Error("INVALID_BLOWFISH");

    x >>>= 24 - 8 * i;
    x &= 0xff;

    return this.SBoxes[i][x] >>> 0;
  }

  f(x) {
    return ((this.s(x, 0) + this.s(x, 1)) ^ this.s(x, 2)) + this.s(x, 3);
  }

  round(a, b, offset) {
    a ^= (this.f(b) >>> 0) ^ (this.PArray[offset] >>> 0);
    a = UINT32(a);
    return a;
  }

  encipher(xl, xr) {
    var _xl = xl,
      _xr = xr;
    var pointer = 0;

    _xl ^= this.PArray[pointer];
    _xl = UINT32(_xl);

    for (const index of Array(8)) {
      _xr = this.round(_xr, _xl, pointer + 1);
      _xl = this.round(_xl, _xr, pointer + 2);
      pointer += 2;
    }

    _xr ^= this.PArray[pointer + 1];
    _xr = UINT32(_xr);

    return {
      xr: _xl,
      xl: _xr,
    };
  }

  decipher(xl, xr) {
    var _xl = xl,
      _xr = xr;
    var pointer = 16;

    _xl ^= this.PArray[pointer + 1];
    _xl = UINT32(_xl);

    for (const index of Array(8)) {
      _xr = this.round(_xr, _xl, pointer);
      _xl = this.round(_xl, _xr, pointer - 1);
      pointer -= 2;
    }

    _xr ^= this.PArray[pointer];
    _xr = UINT32(_xr);

    return {xr: _xl, xl: _xr};
  }

  getOutputLength(length) {
    return length % 8 === 0 ? length : length + (8 - (length % 8));
  }

  encode(stream, offset, length) {
    if (length === 0) throw new Blowfish_Error("ENCODE_STREAM_INVALID_LENGTH");

    let workspace = new Buffer(this.getOutputLength(length));

    stream.copy(workspace, 0, offset);

    for (let x = 0; x < workspace.length; x += 8) {
      const l = workspace.readUInt32LE(x);
      const r = workspace.readUInt32LE(x + 4);
      const {xl, xr} = this.encipher(l, r);

      workspace.writeUInt32LE(xl, x);
      workspace.writeUInt32LE(xr, x + 4);
    }

    return {size: workspace.length, buff: workspace};
  }

  convert(num) {
    let b = new ArrayBuffer(4);
    new DataView(b).setUint32(0, num);
    return Array.from(new Uint8Array(b));
  }

  decode(stream, offset, length) {
    
    console.log({stream, offset, length});
    if (length % 8 != 0 || length == 0) throw new Blowfish_Error("DECODE_STREAM_INVALID_LENGTH");

    let workspace = new Buffer(length);

    stream.copy(workspace, 0, offset);

    for (let x = 0; x < workspace.length; x += 8) {
      const l = workspace.readUInt32LE(x);
      const r = workspace.readUInt32LE(x + 4);

      const {xl, xr} = this.decipher(l, r);

      workspace.writeUInt32LE(xl, x);
      workspace.writeUInt32LE(xr, x + 4);
    }

    return workspace;
  }
}

export default Blowfish;
