import fastify from "fastify";
import {Model} from "objection";
import knex from "knex";
import dbConfig from "./config";
import models from "./models";
import CRUD from "./CRUD";
import {camelCase, startCase} from "lodash";
import oas from "fastify-oas";
import path from "path";
import ipc from "node-ipc";
import system from "systeminformation";

const ipcHandles = {
  // AgentServer: {control: ipc},
  // GatewayServer: {control: ipc},
  //
};

const sendSystemMessage = (module, id, message) =>
  new Promise((resolve, reject) => {
    if (ipcHandles[module]?.control) {
      ipcHandles[module].control.of[module].emit("message", {
        id,
        message,
      });
      resolve();
    } else {
      reject();
    }
  });

class DataServer_Error extends Error {
  constructor(message = "UNKNOWN_ERROR") {
    super();
    this.message = message;
    this.statusCode = 404;
  }
}

class DataServer {
  constructor(config) {
    this.config = config;
  }

  async run() {
    const server = fastify(this.config);
    const connection = knex(dbConfig);

    Model.knex(connection);

    const resolveModel = model => {
      try {
        const model_name = startCase(camelCase(model)).replace(/[^a-zA-Z]/g, "");
        return new CRUD(models[model_name]);
      } catch (e) {
        throw new DataServer_Error(e);
      }
    };

    server.register(oas, {
      routePrefix: "/documentation",
      swagger: {
        info: {
          title: "DataServer",
          description: "Blazing fast CRUD service supporting data pagination, rapid deep data nesting, filtering, sorting, conditional querying",
          version: "1.0.0",
        },
        externalDocs: {
          url: "https://x-z.one",
          description: "Ultimate Developers",
        },
        consumes: ["application/json"],
        produces: ["application/json"],
        servers: [
          {
            url: `http://localhost:${this.config.port}`,
            description: "LOCAL",
          },
          // add your own
        ],
      },
      exposeRoute: true,
    });

    server.route({
      method: "GET",
      url: "/system",
      description: "Query system information",
      handler: async function (request, reply) {
        const info = await system.getAllData();

        reply.send(info);
      },
    });

    // communicate over ipc
    // server.route({
    //   method: "POST",
    //   url: "/ipc",
    //   description: "Send local IPC messages to child processes.",
    //   schema: {
    //     body: {
    //       type: "object",
    //       required: ["module", "message"],
    //       properties: {
    //         module: {
    //           type: "string",
    //           enum: ["GatewayServer", "AgentServer"],
    //         },
    //         id: {
    //           type: "string",
    //         },
    //         message: {
    //           type: "object",
    //         },
    //       },
    //     },
    //     response: {
    //       200: {
    //         type: "object",
    //       },
    //     },
    //   },
    //   handler: async function ({body: {module, id = "DataServer", message}}, reply) {
    //     try {
    //       await sendSystemMessage(module, id, message);
    //       reply.send("OK");
    //     } catch (e) {
    //       reply.send("OFFLINE");
    //     }
    //   },
    // });

    // READ
    server.route({
      method: "POST",
      url: "/:model/search",
      description: "Search a model and relational data",
      schema: {
        querystring: {
          page: {type: "string"},
          limit: {type: "string"},
        },
        params: {
          model: {type: "string"},
        },
        body: {
          type: "object",
          required: ["eager"],
          properties: {
            eager: {
              type: "object",
              properties: {
                $where: {
                  type: "object",
                },
              },
            },
            order: {
              type: ["array", "string"],
            },
            fields: {
              type: "array",
            },
          },
          examples: [
            {
              name: "Nested JSON Querying",
              summary: "Query nested relations, select tables as in example",
              value: {
                eager: {
                  user: {
                    characterLink: {
                      characters: {},
                    },
                    wallet: {},
                  },
                  character: {},
                  $where: {
                    createdAt: {
                      $gt: "2022-04-23T02:30:00",
                    },
                  },
                },
                order: "createdAt desc",
                fields: [
                  "character.CharName16",
                  "user.StrUserID",
                  "user.sec_primary",
                  "user.sec_content",
                  "user.characterLink.characters.CurLevel",
                  "user.characterLink.characters.MaxLevel",
                  "user.characterLink.characters.CharName16",
                  "user.characterLink.characters.NickName16",
                  "user.characterLink.characters.RemainGold",
                  "user.characterLink.characters.RemainSkillPoint",
                  "user.wallet.silk_own",
                  "user.wallet.silk_point",
                  "user.wallet.silk_gift",
                ],
              },
            },
          ],
        },
        response: {
          200: {
            type: "object",
            properties: {
              results: {type: "array"},
              total: {type: "number"},
            },
          },
        },
      },
      handler: async function ({query: {page, limit}, params: {model}, body}, reply) {
        const crud = resolveModel(model);
        const result = await crud.read(body, page || false, limit || 100);
        reply.send(result);
      },
    });

    // CREATE
    server.route({
      method: "POST",
      url: "/:model",
      schema: {
        params: {
          model: {type: "string"},
        },
        body: {
          type: "object",
        },
        response: {
          200: {
            type: "object",
          },
        },
      },
      handler: async function ({params: {model}, body}, reply) {
        const crud = resolveModel(model);
        const result = await crud.create(body);
        reply.send(result);
      },
    });

    // UPDATE
    server.route({
      method: "PUT",
      url: "/:model",
      schema: {
        params: {
          model: {type: "string"},
        },
        body: {
          type: "object",
          properties: {
            where: {
              type: "object",
            },
            payload: {
              type: "object",
            },
          },
        },
        response: {
          200: {
            type: "object",
          },
        },
      },
      handler: async function ({params: {model}, body: {where, payload}}, reply) {
        const crud = resolveModel(model);
        const result = await crud.update(where, payload);
        reply.send(result);
      },
    });

    // DELETE
    server.route({
      method: "DELETE",
      url: "/:model",
      schema: {
        params: {
          model: {type: "string"},
        },
        body: {
          type: "object",
          properties: {
            where: {
              type: "object",
            },
          },
        },
        response: {
          200: {
            type: "object",
          },
        },
      },
      handler: async function ({params: {model}, body}, reply) {
        const crud = resolveModel(model);
        const result = await crud.delete(body);
        reply.send(result);
      },
    });

    try {
      const {port, host} = this.config;

      for await (const ipcKey of Object.keys(ipcHandles)) {
        const {control} = ipcHandles[ipcKey];
        console.log({connections: ipcKey});

        control.config.socketRoot = path.join(__dirname, "..", "..", "..", "socket", "/");
        control.config.logger = data => console.log({ipc: data});

        control.connectTo(ipcKey, () => {
          control.of[ipcKey].on("connect", socket => {
            console.log({
              connected: ipcKey,
            });

            ipcHandles[ipcKey].listener = socket;
          });
        });
      }

      server.listen(port, host);
      server.ready(err => {
        if (err) throw new DataServer_Error(err.message);
        console.log(`DataServer ready ${host}:${port}`);
        server.oas();
      });
    } catch (err) {
      server.log.error(err);
      process.exit(1);
    }
  }
}

export default DataServer;
