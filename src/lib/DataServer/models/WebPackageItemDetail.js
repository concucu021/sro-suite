import {Model} from "objection";

class WebPackageItemDetail extends Model {
  // this table needs idx column
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_DETAIL";
  }

  static get idColumn() {
    return "package_id";
  }
}

export default WebPackageItemDetail;
