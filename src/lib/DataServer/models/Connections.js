import {Model} from "objection";
import Users from "./User";
import Characters from "./Characters";

class Connnections extends Model {
  static get tableName() {
    return "SRO_XPROXY.dbo.connections";
  }

  static get idColumn() {
    return "ID";
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: Users,
        join: {
          from: "SRO_XPROXY.dbo.connections.UserJID",
          to: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
        },
      },
      character: {
        relation: Model.BelongsToOneRelation,
        modelClass: Characters,
        join: {
          from: "SRO_XPROXY.dbo.connections.CharID",
          to: "SILKROAD_R_SHARD.dbo._Char.CharID",
        },
      },
    };
  }

  $beforeInsert() {
    this.created = new Date();
  }

  $beforeUpdate() {
    this.updated = new Date();
  }
}

export default Connnections;