import {Model} from "objection";
import WebPackageItemDetail from "./WebPackageItemDetail";
import WebPackageItemLang from "./WebPackageItemLang";
import WebPackageItemMall from "./WebPackageItemMall";
import WebPackageItemPreview from "./WebPackageItemPreview";

class WebPackageItem extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM";
  }

  static get idColumn() {
    return "package_id";
  }

  static get relationMappings() {
    return {
      detail: {
        relation: Model.HasOneRelation,
        modelClass: WebPackageItemDetail,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.package_id",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_DETAIL.package_id",
        },
      },
      lang: {
        relation: Model.HasOneRelation,
        modelClass: WebPackageItemLang,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.package_id",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_LANG.package_id",
        },
      },
      mall: {
        relation: Model.HasOneRelation,
        modelClass: WebPackageItemMall,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.package_id",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.package_id",
        },
      },
      preview: {
        relation: Model.HasOneRelation,
        modelClass: WebPackageItemPreview,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.package_id",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_PREVIEW.package_id",
        },
      },
    }
  }
}

export default WebPackageItem;
