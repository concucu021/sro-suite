import {Model} from "objection";

class WebItemGiveListDetail extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_GIVE_LIST_DETAIL";
  }

  static get idColumn() {
    return "idx";
  }
}

export default WebItemGiveListDetail;
