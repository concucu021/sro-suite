import {Model} from "objection";
import Connections from "./Connections";
import UserCharacters from "./UserCharacters";
import Wallet from "./Wallet";
import WebItemCertifyKey from "./WebItemCertifyKey";

class User extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.TB_User";
  }

  static get idColumn() {
    return "JID";
  }

  static get relationMappings() {
    return {
      characterLink: {
        relation: Model.HasManyRelation,
        modelClass: UserCharacters,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
          to: "SILKROAD_R_SHARD.dbo._User.UserJID",
        },
      },
      wallet: {
        relation: Model.HasOneRelation,
        modelClass: Wallet,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
          to: "SILKROAD_R_ACCOUNT.dbo.SK_Silk.JID",
        },
      },
      connection: {
        relation: Model.BelongsToOneRelation,
        modelClass: Connections,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
          to: "SRO_XPROXY.dbo.connections.UserJID",
        },
      },
      mall_token: {
        relation: Model.HasOneRelation,
        modelClass: WebItemCertifyKey,
        filter: q => q.orderBy("reg_date", "desc"),
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_CERTIFYKEY.UserJID",
        }
      }
    };
  }
}

export default User;