import {Model} from "objection";
import User from "./User";
import Wallet from "./Wallet";

class WalletQueue extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.SK_SilkChange_BY_Web";
  }

  static get idColumn() {
    return "ID";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["JID", "silk_remain", "silk_offset", "silk_type", "reason"],
      properties: {
        JID: {type: "integer"},
        silk_remain: {type: "integer"},
        silk_offset: {type: "integer"},
        silk_type: {type: "integer"},
        reason: {type: "integer"},
      },
    };
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.SK_SilkChange_BY_Web.JID",
          to: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
        },
      },
      wallet: {
        relation: Model.BelongsToOneRelation,
        modelClass: Wallet,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.SK_SilkChange_BY_Web.JID",
          to: "SILKROAD_R_ACCOUNT.dbo.SK_Silk.JID",
        },
      },
    };
  }
}

export default WalletQueue;
