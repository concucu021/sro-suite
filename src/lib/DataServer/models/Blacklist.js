import {Model} from "objection";
class Blacklist extends Model {
  static get tableName() {
    return "SRO_XPROXY.dbo.blacklist";
  }

  static get idColumn() {
    return "ID";
  }

  $beforeInsert() {
    this.created = new Date();
  }

  $beforeUpdate() {
    this.updated = new Date();
  }
}


export default Blacklist;