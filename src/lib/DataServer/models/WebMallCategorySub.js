import {Model} from "objection";

class WebMallCategorySub extends Model {
  // this table needs idx column
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY_SUB";
  }

  static get idColumn() {
    return "sub_name_us";
  }
}

export default WebMallCategorySub;
