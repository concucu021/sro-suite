import {Model} from "objection";

class WebPackageItemLang extends Model {
  // this table needs idx column
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_LANG";
  }

  static get idColumn() {
    return "package_id";
  }
}

export default WebPackageItemLang;
