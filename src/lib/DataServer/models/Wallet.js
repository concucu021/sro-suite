import {Model} from "objection";
import User from "./User";

class Wallet extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.SK_Silk";
  }

  static get idColumn() {
    return "JID";
  }
  
  static get relationMappings() {
    return {
      characterLink: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.SK_Silk.JID",
          to: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
        },
      },
    };
  }
}

export default Wallet;