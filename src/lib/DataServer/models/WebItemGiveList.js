import {Model} from "objection";

class WebItemGiveList extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_GIVE_LIST";
  }

  static get idColumn() {
    return "idx";
  }
}

export default WebItemGiveList;
