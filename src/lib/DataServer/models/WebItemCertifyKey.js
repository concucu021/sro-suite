import {Model} from "objection";
import User from "./User";

class WebItemCertifyKey extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_CERTIFYKEY";
  }

  static get idColumn() {
    return "idx";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["UserJID", "Certifykey", "ShardID", "reg_date", "CharLevel"],
      properties: {
        UserJID: {type: "integer"},
        Certifykey: {type: "integer"},
        ShardID: {type: "integer"},
        CharLevel: {type: "integer"},
        reg_date: {
          anyOf: [
            {
              type: "string",
              format: "date-time",
            },
            {
              type: "object",
              format: "date-time",
            },
            {
              type: "null",
            },
          ],
        },
      },
    };
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_CERTIFYKEY.UserJID",
          to: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
        },
      },
    };
  }
}

export default WebItemCertifyKey;
