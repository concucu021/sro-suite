import {Model} from "objection";

class WebItemGiveNotice extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_GIVE_NOTICE";
  }

  static get idColumn() {
    return "idx";
  }
}

export default WebItemGiveNotice;
