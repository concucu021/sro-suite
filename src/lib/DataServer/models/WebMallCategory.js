import {Model} from "objection";
import WebMallCategorySub from "./WebMallCategorySub";

// this table needs idx column
class WebMallCategory extends Model {
  static get tableName() {
    return "SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY";
  }

  static get idColumn() {
    return "shop_no";
  }

  static get relationMappings() {
    return {
      subCategories: {
        relation: Model.HasManyRelation,
        modelClass: WebMallCategorySub,
        filter: q => q.orderBy("sub_no", "asc"),
        join: {
          from: "SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY.shop_no",
          to: "SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY_SUB.ref_no",
        },
      },
    };
  }

 
}

export default WebMallCategory;
