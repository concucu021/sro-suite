import {Model} from "objection";
import Character from "./Characters";
import User from "./User";

class UserCharacters extends Model {
  static get tableName() {
    return "SILKROAD_R_SHARD.dbo._User";
  }

  static get idColumn() {
    return "CharID";
  }

  static get relationMappings() {
    return {
      characters: {
        relation: Model.HasManyRelation,
        modelClass: Character,
        join: {
          from: "SILKROAD_R_SHARD.dbo._User.CharID",
          to: "SILKROAD_R_SHARD.dbo._Char.CharID",
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "SILKROAD_R_SHARD.dbo._User.UserJID",
          to: "SILKROAD_R_ACCOUNT.dbo.TB_User.JID",
        },
      },
    };
  }
}

export default UserCharacters;