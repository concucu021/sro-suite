import dotenv from "dotenv";

dotenv.config();

const {DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DIALECT, DB_INSTANCE_NAME, DB_NAME} = process.env;

const connection = {
  host: DB_HOST || "127.0.0.1",
  port: DB_PORT || 1433,
  user: DB_USERNAME || "sa",
  password: DB_PASSWORD || "",
  database: DB_NAME || "SRO_XPROXY",
  options: DB_INSTANCE_NAME
    ? {
        instanceName: DB_INSTANCE_NAME,
      }
    : {},
};

export default {
  client: DB_DIALECT || "mssql",
  useNullAsDefault: true,
  connection,
};
