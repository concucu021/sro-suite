import {createServer, BlockList} from "net";
import {fork} from "child_process";
import ipc from "node-ipc";
import API from "@lib/DataAPI";
import {FastRateLimit as rateLimit} from "fast-ratelimit";
import path from "path";

const {data: dataService} = API;
const WORKERS_PATH = `${__dirname}/workers/`;

const limitter = new rateLimit({
  threshold: 14,
  ttl: 3,
});

const blockList = new BlockList();

class AsyncServer {
  constructor(config) {
    Object.assign(this, {
      config: {
        debug: process.env.NODE_ENV == "development",
        ...config,
        BANNED_COUNTRY_CODES: new Set(config.BANNED_COUNTRY_CODES || []),
      },
      workers: {},
    });

    this.ipc = ipc;
    this.hookEventManager();
  }

  hookEventManager() {
    const eventWorkerPath = this.config.debug ? `${WORKERS_PATH}/EventManager/index.dev` : `${WORKERS_PATH}/EventManager/index`;
    const eventWorkerConfig = JSON.stringify({
      context: this.config.module,
    });

    this.eventsManager = fork(eventWorkerPath, [eventWorkerConfig]);

    this.eventsManager.on("message", message => {
      switch (message.type) {
        case "global":
          this.messageWorkers("EventManager", message);
          break;

        default:
          console.log(`unknown EventManager msg`, message);
          break;
      }
    });
  }

  generateUniqueId(input) {
    return Buffer.from(input, "utf-8").toString("base64");
  }

  getWorker(id) {
    if (this.workers[id]) {
      return this.workers[id];
    }
  }

  messageWorkers(sender, message, includeSelf = true, targets = []) {
    let keys = includeSelf ? Object.keys(this.workers) : Object.keys(this.workers).filter(i => i !== sender);

    if (targets.length) keys = keys.filter(i => this.workers[i] && targets.includes(i));

    console.log(`messageWorkers`, {
      sender,
      message,
      includeSelf,
      targets,
    });

    for (const key of keys) {
      this.workers[key].send({
        code: 2,
        data: message,
      });
    }
  }

  closeWorker(id) {
    if (this.workers[id]) {
      this.workers[id].kill();
      delete this.workers[id];
    }
  }

  block(socket) {
    blockList.addAddress(socket.remoteAddress);
    return socket.destroy();
  }

  async run() {
    const self = this;

    self.server = createServer(async socket => {
      console.log({connected: socket.remoteAddress});

      if (blockList.check(socket.remoteAddress)) {
        console.log(`Disconnected [${socket.remoteAddress}] because of blacklist`);
        return socket.destroy();
      }

      // console.log({limit: limitter.consumeSync(socket.remoteAddress)})

      if (limitter.consumeSync(socket.remoteAddress)) {
        const {
          data: {total},
        } = await dataService.post("/blacklist/search", {
          eager: {
            $where: {
              remote: socket.remoteAddress,
            },
          },
        });

        if (total > 0) {
          self.block(socket);
        }

        const id = self.generateUniqueId(`${socket.remoteAddress}:${socket.remotePort}`);
        const workerInstance = self.config.debug ? `${WORKERS_PATH}/Client/index.dev` : `${WORKERS_PATH}/Client/index`;
        const workerConfig = JSON.stringify({
          config: self.config,
          info: {
            id,
            ip: socket.remoteAddress,
            port: socket.remotePort,
          },
        });

        self.workers[id] = fork(workerInstance, [workerConfig]);

        // S -> C
        self.workers[id].on("message", message => {
          switch (message.type) {
            case "global":
              // console.log({message});
              self.messageWorkers(id, message, message.data?.includeSelf, message.data?.targets);
              break;
            case "buffer":
              socket.write(Buffer.from(message.data));
              break;
            case "disconnect":
              socket.destroy();
              self.closeWorker(id);
              break;
          }
        });

        self.workers[id].on("error", err => self.closeWorker(id));

        // C -> S
        socket.on("data", buffer => {
          if (self.workers[id])
            self.workers[id].send({
              code: 1,
              data: buffer,
            });
        });

        socket.on("error", _error => {
          socket.destroy();
          if (self.workers[id])
            self.workers[id].send({
              code: 0,
            });
        });

        socket.on("close", () => {
          socket.destroy();
          if (self.workers[id])
            self.workers[id].send({
              code: 0,
            });
        });
      } else {
        console.log(`Blocked [${socket.remoteAddress}].`);
        return self.block(socket);
      }
    });

    self.ipc.config.id = self.config.module;
    self.ipc.config.socketRoot = path.join(__dirname, "..", "..", "socket", "/");

    self.ipc.serve(() => {
      console.log({
        ["ipc:ready"]: self.ipc.config.id,
      });

      self.ipc.server.on("message", ({id, message}) => {
        console.log("IPC", {id, message});

        switch (message.type) {
          case "global":
            self.messageWorkers(id, message, message.data?.includeSelf || false, message.data?.targets);
            break;
          case "disconnect":
            if (self.workers[id])
              self.workers[id].send({
                code: 0,
              });
            break;
        }
      });
    });

    self.ipc.server.start();

    self.server.listen(self.config.LOCAL.PORT, self.config.LOCAL.HOST);

    console.log({
      [self.config.module]: {
        local: self.config.LOCAL,
        remote: self.config.REMOTE,
      },
    });
  }
}

export default AsyncServer;
