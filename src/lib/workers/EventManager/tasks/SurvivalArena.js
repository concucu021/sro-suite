import sendMessage from "../util/sendMessage";
import {testPacket, playerNotice} from "@lib/utils/Packets";
import {uniqBy} from "lodash";

const delay = ms => new Promise(res => setTimeout(res, ms));

const controller = async ({api: {data: dataService}}) => {
  console.log({
    script_init: new Date(),
  });

  const {packet: start_notice} = playerNotice("survival arena is about to start in 5 minutes", 7);

  sendMessage(start_notice);

  await delay(2 * 60 * 1000); // this should be 2 minutes?

  const {packet: start_notice_2} = playerNotice("survival arena is about to start in 3 minutes", 7);

  sendMessage(start_notice_2);
};
export default {
  context: "AgentServer",
  schedules: [
    {
      minute: 30,
    },
  ],
  controller,
};
