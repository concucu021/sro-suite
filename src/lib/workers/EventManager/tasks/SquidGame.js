const controller = async () => {
  return true;
};

export default {
  context: "AgentServer",
  schedules: [
    {
      hour: 18,
      second: 30,
    },
  ],
  controller,
};
