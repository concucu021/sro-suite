import sendMessage from "../util/sendMessage";
import {testPacket, playerNotice} from "@lib/utils/Packets";
import {uniqBy} from "lodash";

const controller = async ({api: {data: dataService}}) => {
  //   const {
  //     data: {results: connections},
  //   } = await dataService.post("/connections/search", {
  //     eager: {
  //       $where: {
  //         CharID: {
  //           $in: [72, 71],
  //         },
  //       },
  //     },
  //     order: "created desc",
  //     limit: 1,
  //   });

  //   const items = uniqBy(connections, "CharID");

  //   for (const connection of items) {
  //     if (connection?.connection_id) {
  //       const {packet: test_packet} = playerNotice(`You noob`); //testPacket(12496, [12, 30, 1, 0, 154, 153, 25, 66, 1, 0, 240, 66]);

  //       const payload = {
  //         ...test_packet,
  //         targets: [connection?.connection_id],
  //       };

  //       console.log({
  //         connection,
  //         payload,
  //       });

  //       sendMessage(payload);
  //     }
  //   }

  const {packet: placement_packet} = playerNotice(`TEST`);
  sendMessage(placement_packet);
};

export default {
  context: "AgentServer",
  schedules: [
    {
      second: 30,
    },
  ],
  controller,
};
