import {playerNotice} from "@lib/utils/Packets";
import sendMessage from "../util/sendMessage";
import {sample, capitalize, uniqBy, shuffle} from "lodash";
import TriviaQuestions from "../config/TriviaQuestions";

const delay = ms => new Promise(res => setTimeout(res, ms));

const controller = async ({api: {data: dataService}}) => {
  const time = new Date();

  const {question, answer, special = false} = sample(shuffle(TriviaQuestions));

  console.log({status: `EVENT_STARTED`, question, answer});

  const [
    {packet: intro_packet},
    {packet: question_packet},
    {packet: answer_packet},

    {packet: count_3_packet},
    {packet: count_2_packet},
    {packet: count_1_packet},
    {packet: time_over_packet},
    //
  ] = [
    playerNotice(`[Trivia] Answer the question faster than others and win silk - you have 30 seconds.`),
    playerNotice(`[Trivia] ${special ? "Special " : ""}Question: ${question}`),
    playerNotice(
      `[Trivia] Correct answer: ${answer
        .split(" ")
        .map(i => capitalize(i))
        .join(" ")}`,
    ),

    // countdown stuff:
    playerNotice(`[Trivia] 3 seconds to go...`),
    playerNotice(`[Trivia] 2 seconds to go...`),
    playerNotice(`[Trivia] Last second...`),
    playerNotice(`[Trivia] aand.. Time's over!`),
  ];

  sendMessage(intro_packet);

  await delay(1000); // 1s later

  sendMessage(question_packet);
  await delay(28000); // 28s later

  // countdown:
  sendMessage(count_3_packet);
  await delay(1000);
  sendMessage(count_2_packet);
  await delay(1000);
  sendMessage(count_1_packet);
  await delay(1000);
  sendMessage(time_over_packet);

  // messages from the event start:
  const {
    data: {results},
  } = await dataService.post("/chat-logs/search", {
    eager: {
      user: {
        wallet: {},
        connection: {},
      },
      character: {},
      $where: {
        created: {
          $gt: time,
        },
      },
    },
    order: "created asc",
    fields: ["user.JID", "character.CharName16", "user.wallet.silk_own"],
  });

  const correctly_answered = uniqBy(
    results.filter(i => i.message.toLowerCase().includes(answer.toLowerCase())),
    "CharID",
  );

  const rewards = {
    first: {
      winner: correctly_answered[0] || false,
      amount: special ? 30 : 15,
    },
    second: {
      winner: correctly_answered[1] || false,
      amount: special ? 20 : 10,
    },
    third: {
      winner: correctly_answered[2] || false,
      amount: special ? 10 : 5,
    },
  };

  sendMessage(answer_packet);

  if (correctly_answered.length > 0) {
    for await (const place of Object.keys(rewards)) {
      const {winner, amount} = rewards[place];

      if (winner) {
        const {packet: placement_packet} = playerNotice(`[Trivia] ${capitalize(place)} [${amount} silk]: ${winner.character.CharName16} - congratulations!`);
        sendMessage(placement_packet);

        await Promise.all([
          dataService.post("/wallet-queue", {
            JID: winner.UserJID,
            silk_remain: winner.user.wallet.silk_own + amount,
            silk_offset: amount,
            silk_type: 0,
            reason: 0,
          }),
          dataService.put("/wallet", {
            where: {
              JID: winner.UserJID,
            },
            payload: {
              silk_own: winner.user.wallet.silk_own + amount,
            },
          }),
        ]);
      }
    }
  } else {
    const {packet: no_win_packet} = playerNotice(`[Trivia] No winners - try harder at the next time!`);
    sendMessage(no_win_packet);
  }

  console.log({status: `EVENT_END`, rewards});

  return true;
};

export default {
  context: "AgentServer",
  schedules: [
    {
      minute: 3,
    },
  ],
  controller,
};
