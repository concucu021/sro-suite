import scheduler from "node-schedule";
import * as tasks from "./tasks";
import api from "@lib/DataAPI";

const config = JSON.parse(process.argv[2]);

async function initEventManager() {
  let jobs = [];

  for await (const task of Object.keys(tasks)) {
    const {context, controller, schedules} = tasks[task];

    if (context === config.context) {
      schedules.map(schedule => {
        console.log({scedule_register: schedule, task});
        jobs.push(
          scheduler.scheduleJob(schedule, async () => {
            console.log(`[${config.context}] event_start = "${task}"`, {timestamp: new Date()});
            try {
              await controller({api});
            } catch (error) {
              console.log(`[${config.context}] event_error = "${task}"`, {timestamp: new Date(), error });
            } finally {
              console.log(`[${config.context}] event_end = "${task}"`, {timestamp: new Date()});
            }
          }),
        );
      });
    }
  }

  console.log({
    status: "EVENT_MANAGER_STARTED",
    total_jobs: jobs.length,
    config,
  });
}

initEventManager();
