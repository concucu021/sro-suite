const sendMessage = (
    packet,
    targets = [],
) => {
    process.send({
        type: "global",
        data: {
            opcode: packet.opcode,
            data: packet.data,
            includeSelf: false,
            targets,
        },
    });
}

export default sendMessage;