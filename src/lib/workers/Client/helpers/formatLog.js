const formatLog = ({combine, timestamp, prettyPrint, colorize, splat, simple, errors, printf}) =>
  combine(
    colorize(),
    timestamp(),
    prettyPrint(),
    splat(),
    simple(),
    errors(),
    printf(({timestamp, level, message, ...data}) => {
      let json = JSON.stringify(data, null, 2);
      json = json === "{}" ? "" : json;
      return `[${timestamp}] ${level} - ${message} ${json}`;
    }),
  );

export default formatLog;
