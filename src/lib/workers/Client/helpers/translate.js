const translate = op => `0x${op.toString(16).toUpperCase()}`;
export default translate;
