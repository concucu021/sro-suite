async function onAgentDisconnect({proxy: {data: dataService}}, session) {
  console.log({
    onAgentDisconnect: session,
  });

  if (session)
    await dataService.delete(`/connections`, {
      connection_id: session.id,
      context: session.context,
    });
}

export default onAgentDisconnect;
