import {Security, stream} from "@root/silkroad-security";
import {Socket} from "net";
import {createLogger, format, transports} from "winston";
import {translate} from "./helpers";
import api from "@lib/DataAPI";
import NodeCache from "node-cache";
import moment from "moment";
import path from "path";
import * as ctrl from "@control";
import * as helper from "./helpers";
import {FastRateLimit as rateLimit} from "fast-ratelimit";

const isDev = process.env?.NODE_ENV === "development";

// limit payload size (bytes):
const maxPayloadSize = 192;
//debug only: opcodes to not log:
const skipOpcodes = [
  8194,
  8193,
  28788,
  28869,
  28748,
  28741,
  28708,
  28780,
  28724,
  29698, //
];

const limitter = new rateLimit({
  threshold: 65, // packets
  ttl: 5, // seconds
});

const init_timestamp = moment().format("MM-DD-YYYY");
const {formatLog} = helper;
const {config, info} = JSON.parse(process.argv[2]);
const filename = path.join(__dirname, "..", "..", "..", "..", "logs", `${config.module.toLowerCase()}`, `${init_timestamp}_${info.ip}${isDev ? "-dev" : ""}.log`);

const logger = createLogger({
  transports: [new transports.File({filename})],
});

if (process.env.NODE_ENV !== "production") {
  logger.add(
    new transports.Console({
      format: formatLog(format),
    }),
  );
}

const memory = new NodeCache();
const socket = new Socket();

const security = {
  client: new Security(),
  remote: new Security(),
};

const disconnect = async () => {
  await api.data.delete(`/connections`, {
    data: {
      connection_id: info.id,
      context: config.module,
    },
  });

  logger.log("info", "disconnect", info);

  process.send({type: "disconnect"});
};

const middlewares = Object.keys(config.middlewares).reduce(
  (endpoints, mw) => ({
    ...endpoints,
    [mw]: Object.keys(config.middlewares[mw]).reduce(
      (handles, opcode) => ({
        ...handles,
        [opcode]: ctrl[config.middlewares[mw][opcode]],
      }),
      {},
    ),
  }),
  {},
);

let record_whitelist = [];

async function handlePacket(sender, packet) {
  let target = sender === "global" ? sender : sender === "client" ? "remote" : "client";

  security[sender].receive(Buffer.from(packet).toJSON().data);

  const incomingStream = security[sender].processIncomingStream();

  for await (const incomingPacket of incomingStream) {
    const payloadSize = incomingPacket.data.length || 0;

    // Temporarily record all non-whitelisted C->S opcodes:
    if (!config.whitelist.includes(incomingPacket.opcode) && !skipOpcodes.includes(incomingPacket.opcode) && target !== "client") {
      record_whitelist.push(translate(incomingPacket.opcode));
      logger.log("info", "whitelist_dump", {opcodes: record_whitelist});

      config.whitelist.push(incomingPacket.opcode);
    }

    if ((target === "remote" && payloadSize <= maxPayloadSize && config.whitelist.includes(incomingPacket.opcode)) || target === "client") {
      // Log all packets:
      if (!skipOpcodes.includes(incomingPacket.opcode))
        logger.log("info", "packet", {
          opcode: translate(incomingPacket.opcode),
          operation: incomingPacket.opcode,
          sender,
          target,
          data: incomingPacket,
          ascii: Buffer.from(incomingPacket.data).toString("ascii"),
        });

      const middleware = middlewares[sender][incomingPacket.opcode] || false;

      const runMiddleware = middleware
        ? await middleware(
            {
              stream,
              config,
              api,
              memory,
              info,
              logger,
            },
            incomingPacket,
            sender,
            target,
          )
        : [
            {
              packet: incomingPacket,
              target,
            },
          ];

      for (const {packet: _packet, exit: _exit, target: _target} of runMiddleware) {
        if (_exit) return disconnect();

        if (_packet) {
          const {opcode, data, encrypted, massive} = _packet;
          let security_target = _target || target;

          if (security_target.startsWith("global") && target !== "remote") {
            process.send({
              type: "global",
              data: {
                opcode,
                data,
                includeSelf: security_target !== "global:notSelf",
              },
            });
          } else {
            security[target].send(opcode, data, encrypted || false, massive || false);
          }
        }
      }
    } else {
      if (!skipOpcodes.includes(incomingPacket.opcode))
        logger.log("info", "not_whitelisted", {
          opcode: translate(incomingPacket.opcode),
          operation: incomingPacket.opcode,
          incomingPacket,
          sender,
        });
    }
  }

  // Deliver the resulting packets:
  const outgoingStream = security[target].processOutgoingStream();

  outgoingStream.map(outgoingPacket => {
    switch (target) {
      case "remote":
        socket.write(Buffer.from(outgoingPacket));
        break;
      default:
        process.send({
          type: "buffer",
          data: outgoingPacket,
        });
        break;
    }
  });
}

async function handleGlobalMessage({data: {opcode, data, direction}}) {
  console.log(`handleGlobalMessage`, {opcode, data, direction});

  security[direction || "client"].Send(opcode, data.filter(i => i !== null), false, false);

  const outgoingStream = security[direction || "client"].processIncomingStream();

  outgoingStream.map(packet => {
    switch (direction) {
      case "remote":
        socket.write(Buffer.from(packet));
        break;
      default:
        process.send({
          type: "buffer",
          data: packet,
        });
        break;
    }
  });
}

security.client.generateHandshake(true, true, true); 

// Connect to the remote server:
socket.connect({
  host: config.REMOTE.HOST,
  port: config.REMOTE.PORT,
  onread: {
    buffer: Buffer.alloc(8192),
  },
});

socket.on("connect", async _ => {
  await api.data.post("/connections", {
    connection_id: info.id,
    context: config.module,
    remote: info.ip,
    port: info.port,
  });
});

// Server -> Client
socket.on("data", async data => await handlePacket("remote", data));

// Client -> Server
process.on("message", async ({code, data}) => {
  // console.log({data});

  if (limitter.consumeSync("client")) {
    switch (code) {
      case 1:
        await handlePacket("client", data);
        break;
      case 2: // global messaging
        await handleGlobalMessage(data);
        break;
      case 0: // disconnect from master
      default:
        await disconnect();
        break;
    }
  } else {
    await disconnect();
  }
});

// Reasons for disconnect:
socket.on("error", async error => {
  await disconnect();
});

socket.on("close", async () => {
  await disconnect();
});
