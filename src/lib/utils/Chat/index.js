import * as control from './commands';

const gm_access = {
    sec_primary: [1, 2, 3, 4, 5, 6],
    sec_content: [1, 2, 3, 4, 5, 6],
};

const user_access = {
    sec_primary: [1, 2, 3, 4, 5, 6],
    sec_content: [1, 2, 3, 4, 5, 6],
}

const commands = {
    "msg": { // command name !msg
        command: control.globalMessage, // command function
        chatTypes: ['GM Public'], // which chat it applies to
        access: gm_access,
    },
    "time": { // !time
        command: control.getServerTime, // command function
        chatTypes: ['GM Public', 'Public'], // which chat it applies to
        access: user_access,
    },
    "scene": {
        command: control.triggerCutscene, // command function
        chatTypes: ['GM Public'], // which chat it applies to
        access: gm_access,
    },
    "test": {
        command: control.testCommand,
        chatTypes: ['Guild'],
        access: user_access,
    } //!test 
}

class ChatCommands {
    constructor(props) {
        this.props = props;
    }

    async resolve(i) {
        try {
            const index = commands[i];

            if (!index) throw "COMMAND_NOT_FOUND";

            const { command, access, chatTypes } = index;

            if (!chatTypes.includes(this.props.chatType)) throw "INVALID_CHAT_TYPE";
            if (!access?.sec_primary?.includes(this.props.sec_primary) || !access?.sec_content?.includes(this.props.sec_content)) throw "ACCESS_DENIED";

            return await command(this.props);
        } catch (e) {
            return false;
        }
    }
}

export default ChatCommands;