import {playerNotice} from "@lib/utils/Packets";

async function GlobalMessage({CharName16, params}) {
  const notice = playerNotice(`${CharName16}: ${params.join(" ")}`);
  return [
    notice,
    {
      ...notice,
      target: "global:notSelf",
    },
  ];
}

export default GlobalMessage;
