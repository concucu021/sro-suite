import {playerNotice} from "@lib/utils/Packets";
import moment from "moment";

async function GetServerTime() {
  const time = moment().utc().format("HH:mm:ss");
  const notice = playerNotice(`Time now: ${time}`);

  return [notice];
}

export default GetServerTime;
