import {triggerCutscene, playerNotice} from "@lib/utils/Packets";

async function GetServerTime({params}) {
  const [scriptName, scriptLength] = params;

  console.log({scriptName, scriptLength});

  return [triggerCutscene(scriptName, scriptLength), playerNotice(`STARTED: ${scriptLength} (${scriptLength}s)`)];
}

export default GetServerTime;
