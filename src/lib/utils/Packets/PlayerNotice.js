import {stream} from "@root/silkroad-security";

export default (message = "", type = 7) => {
  const packet = new stream.writer();
  packet.uint8(type);
  packet.string(message);

  return {
    packet: {
      opcode: 0x3026,
      data: packet.toData(),
    },
    target: "client",
  };
};
