import {stream} from "@root/silkroad-security";

export default (scriptName = "", duration = 20) => {
  const packet = new stream.writer();

  packet.string(scriptName.toLowerCase());
  packet.uint8(parseInt(duration));

  return {
    packet: {
      opcode: 0x3ca2,
      data: packet.toData(),
    },
    target: "client",
  };
};
