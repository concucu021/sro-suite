export default (opcode, data) => {
  return {
    packet: {
      opcode,
      data,
    },
    target: "client",
  };
};
