import fastify from "fastify";
import {Model} from "objection";
import knex from "knex";
import dbConfig from "@lib/DataServer/config";
import models from "@lib/DataServer/models";
import CRUD from "@lib/DataServer/CRUD";
import WebPackageItem from "@lib/DataServer/models/WebPackageItem";
import path from "path";
import fs, {createWriteStream} from "fs";
import axios from "axios";

const db = knex(dbConfig);
Model.knex(db);

async function downloadFile(fileUrl, outputLocationPath) {
  console.log({fileUrl, outputLocationPath});
  const writer = createWriteStream(outputLocationPath);

  return axios({
    method: "get",
    url: fileUrl,
    responseType: "stream",
  }).then(response => {
    return new Promise((resolve, reject) => {
      response.data.pipe(writer);
      let error = null;
      writer.on("error", err => {
        error = err;
        writer.close();
        reject(err);
      });
      writer.on("close", () => {
        if (!error) {
          resolve(true);
        }
      });
    });
  });
}

async function init() {
  const model = new CRUD(WebPackageItem);
  const data = await model.read({
    eager: {},
    fields: ["package_code"],
  });

  let missing = [];

  for await (const {dbo} of data?.results) {
    try {
      const fileName = `${dbo}.jpg`;
      const location = `http://silkroadcpimg.joymax.com:27001/property/silkroad/itemlist_pac/${fileName}`;
      const destination = path.join(__dirname, "lib", "WebAppServer", "public", "images", "items", fileName);

      if (!fs.existsSync(destination)) {
        // await downloadFile(location, destination);

        // console.log({package_code: "downloaded", location, destination});
        missing.push(dbo);
      }
    } catch (e) {
      console.log("Failed to download: ", dbo);
    }
  }

  console.log(missing);

  process.exit(0);
}

init();
