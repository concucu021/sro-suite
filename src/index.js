import dotenv from "dotenv";
import findProcess from "find-process";
import * as service from "@service";

dotenv.config();

const index = process.env.MODULE || false;
const isDev = process.env.NODE_ENV == "development";

const exit = () => {
  console.log(`[${index}]->exit`);
  process.exit(0);
};

const init = () => {
  try {
    index && new service[index]().run();
  } catch (error) {
    console.log(`[${index}]->(error)->${error}`);
  }
};

const check_dead_process = async error => {
  if (error.code == "EADDRINUSE") {
    const [dead] = await findProcess("port", error.port);

    if (dead) {
      console.log(`Port not open [${error.port}]->reinit`);
      process.kill(dead.pid);
      init();
    } else {
      init();
    }
  }
};

process.on("unhandledRejection", async (error, promise) => {
  if (isDev) {
    console.log({index, unhandledRejectionError: error});
    try {
      await check_dead_process(error);
    } catch (e) {
      console.log({index, unhandledRejectionError: e});
      exit();
    }
    // exit();
  } else {
    promise.catch();
  }
});

process.on("uncaughtException", async error => {
  if (isDev) console.log({uncaughtException: error});
  await check_dead_process(error);
});

init();
