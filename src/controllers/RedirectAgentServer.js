import redirects from "@config/redirects";

async function RedirectAgentServer({stream, logger}, packet) {
  try {
    const {writer, reader} = stream;

    const read = new reader(packet.data);
    const status = read.uint8();

    if (status == 1) {
      const token = read.uint32();
      const host = read.string();
      const port = read.uint16();
      const auth_state = read.uint8(); // no idea what this 1 means

      const redirect = redirects[`${host}:${port}`];

      logger.log("info", "agent_redirect", {
        token,
        host,
        port,
        auth_state,
        redirect,
      });

      const write = new writer();
      write.uint8(status);
      write.uint32(token);
      write.string(redirect.host);
      write.uint16(redirect.port);
      write.uint8(auth_state);

      return [
        {
          packet: {
            ...packet,
            data: write.toData(),
          },
        },
      ];
    }

    return [{packet}];
  } catch (e) {
    return [{
      exit: true,
    }];
  }
}

export default RedirectAgentServer;
