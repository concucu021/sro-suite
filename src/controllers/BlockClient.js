import {playerNotice} from "@lib/utils/Packets";

async function Block({stream: {writer}, logger}, packet) {
  try {
    logger.log("info", "blocked_packet", {
      packet,
    });

    const write = new writer();
    write.uint8(7);
    write.string(`That's your dog's food.`);

    return [
      {
        packet: {
          opcode: 0x3026,
          data: write.toData(),
        },
        target: "client",
      },
    ];
  } catch (e) {
    console.log(`BlockClient_ERR`, e);
    return [];
  }
}

export default Block;
