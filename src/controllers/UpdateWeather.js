import {sample} from "lodash";

async function UpdateWeather(
  {
    config: {
      weather: {type, intensity},
    },
    stream: {writer},
  },
  packet,
) {
  try {
    const selectedWeather = sample(type);
    const selectedIntensity = sample(intensity);

    const write = new writer();

    write.uint8(selectedWeather);
    write.uint8(selectedIntensity);

    return [
      {
        packet: {
          ...packet,
          data: write.toData(),
        },
      },
    ];
  } catch (e) {
    return [{packet}];
  }
}

export default UpdateWeather;
