const chat_types = {
  1: "ALL",
  2: "PM",
  3: "ALLGM",
  4: "PARTY",
  5: "GUILD",
  6: "GLOBAL",
  7: "NOTICE",
  9: "STALL",
  11: "UNION",
  13: "NPC",
  16: "ACADEMY",
};

async function Debug({logger, stream: {reader}}, packet) {
  try {
    const read = new reader(packet.data);

    const type = chat_types[read.uint8()];

    let unique_id = false,
      sender_name = false;

    if (
      [
        "ALL",
        "ALLGM",
        "NPC",
        //
      ].includes(type)
    ) {
      unique_id = read.uint8();
    } else if (
      [
        "PM",
        "PARTY",
        "GUILD",
        "GLOBAL",
        "STALL",
        "UNION",
        "ACADEMY",
        //
      ].includes(type)
    ) {
      sender_name = read.string();
    }

    const message = read.string();

    console.log({
      type,
      unique_id,
      sender_name,
      message,
    });

    return [
      {
        packet,
      },
    ];
  } catch (e) {
    return [];
  }
}

export default Debug;
