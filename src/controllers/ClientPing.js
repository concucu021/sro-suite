async function ClientPing({memory, logger, config: {timeUntilAwayStatus}, stream: {writer}}, packet) {
  try {
    const [isAwayStatus, lastActivityTime, actualTime] = [memory.get("isAwayStatus") || false, memory.get("lastActivityTime") || new Date(), new Date()];

    const diff = Math.abs(actualTime - lastActivityTime) / 1000;

    // Char is afk?
    if (!isAwayStatus && diff >= timeUntilAwayStatus) {
      // logger.log('info', 'gone_afk', {
      //     isAwayStatus,
      //     lastActivityTime,
      //     actualTime,
      //     diff,
      // });

      memory.set("isAwayStatus", true);

      const update_afk = () => {
        const write = new writer();
        write.uint8(2);

        return {
          packet: {
            opcode: 0x7402,
            data: write.toData(),
          },
          target: "remote",
        };
      };

      return [
        {
          packet,
          target: "remote",
        },
        update_afk(),
      ];
    }

    return [
      {
        packet,
      },
    ];
  } catch (e) {
    console.log(`ClientPING_ERR`, e);
    return [];
  }
}

export default ClientPing;
