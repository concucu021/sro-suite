import {playerNotice} from "@lib/utils/Packets";

const transformNumber = (value, dec = 0) => {
  const suffixes = ["K", "M", "B", "T", "P", "E"];

  if (!value) {
    return "some";
  }

  if (Number.isNaN(value)) {
    return "some";
  }

  if (value < 1000) {
    return value;
  }

  const exp = Math.floor(Math.log(value) / Math.log(1000));

  const returnValue = (value / Math.pow(1000, exp)).toFixed(dec) + suffixes[exp - 1];

  return returnValue;
};

async function GoriExchange({config: {goriExchangeGoldMinimum}, memory, api: {data: DataService}, logger}, packet) {
  try {
    const CharID = memory.get("CharID");

    // Retrieve actual char information

    const {
      data: {
        results: [{RemainGold, CharName16}],
      },
    } = await DataService.post(`/characters/search`, {
      eager: {
        $where: {
          CharID,
        },
      },
    });

    if (parseInt(RemainGold) >= goriExchangeGoldMinimum) {
      return [{packet}];
    } else {
      logger.log("info", "gori_blocked", {
        CharID,
        RemainGold,
        CharName16,
        goriExchangeGoldMinimum,
      });

      return [
        playerNotice(`Have at least ${transformNumber(goriExchangeGoldMinimum)} gold in your bag.`),
        //
      ];
    }
  } catch (e) {
    console.log(`GoriExchange_ERR`, e);
    return [];
  }
}

export default GoriExchange;
