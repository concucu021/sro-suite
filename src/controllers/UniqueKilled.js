import { playerNotice } from "@lib/utils/Packets";

async function UniqueKilled({logger, config: {UNIQUES}, stream: {reader, writer}, memory, api: {data: dataService}}, packet) {
  try {
    const current_charname = memory.get("CharName16"); //whoami
    const read = new reader(packet.data);

    if (read.uint8() == 6) {
      const uniqueId = read.uint16();
      const arg1 = read.uint8();
      const arg2 = read.uint16();
      const killer_name = read.string();

      if (killer_name == current_charname) {
        const unique_config = UNIQUES[uniqueId] || false;
        const UserJID = memory.get("UserJID");
        const CharID = memory.get("CharID");

        // Retrieve actual char information

        const [
          {
            data: {
              results: [
                {
                  CurLevel,
                  characterLink: {
                    user: {
                      wallet: {silk_own},
                    },
                  },
                },
              ],
            },
          },
        ] = await Promise.all([
          dataService.post("/characters/search", {
            eager: {
              characterLink: {
                user: {
                  wallet: {},
                },
              },
              $where: {
                CharID,
              },
            },
          }),
          dataService.post("/achievements", {
            UserJID,
            CharID,
            type: "hunt",
            status: "completed",
            meta: {
              uniqueId,
              unique_config,
            },
          }),
        ]);

        if (unique_config && CurLevel <= unique_config.cap) {
          // Dispatch silk reward

          await Promise.all([
            dataService.post("/wallet-queue", {
              JID: UserJID,
              silk_remain: silk_own + unique_config.reward,
              silk_offset: unique_config.reward,
              silk_type: 0,
              reason: 0,
            }),
            dataService.put("/wallet", {
              where: {
                JID: UserJID,
              },
              payload: {
                silk_own: silk_own + unique_config.reward,
              },
            }),
          ]);

          // packets go in order as specified (one object per packet):
          return [
            {
              // return original packet s->c
              packet,
              target: "client",
            },
            playerNotice(`Great! You got ${unique_config.reward} silk for killing [${unique_config.name}]`),
          ];
        }
      }
    }

    return [{packet, target: "client"}];
  } catch (e) {
    console.log(`[UniqueKilled]->error:`, e);
    return [{packet}];
  }
}

export default UniqueKilled;
