async function GlobalChat({stream: {reader}, memory, logger}, packet) {
  try {
    const read = new reader(packet.data);
    const type = read.uint8();
    const CharName16 = memory.get("CharName16");

    if (type === 6 && read.string() == CharName16) {
      const message = read.string();

      // logger.log('info', 'global_chat', {
      //     sender: CharName16,
      //     msg: message,
      // });
    }

    return [{packet}];
  } catch (e) {
    console.log(`GlobalChat_ERR`, e);
    return [];
  }
}

export default GlobalChat;
