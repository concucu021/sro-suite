import ChatCommands from "@lib/utils/Chat";

async function ChatRequest({logger, config: {CHAT_TYPES}, stream: {reader}, memory, api: {data: dataService}}, packet) {
  try {
    const read = new reader(packet.data);
    const type = read.uint8();

    const index = read.uint8();
    const linkedItemsCount = read.uint8();

    const UserJID = memory.get("UserJID");
    const CharID = memory.get("CharID");
    const CharName16 = memory.get("CharName16");
    const sec_primary = memory.get("sec_primary");
    const sec_content = memory.get("sec_content");

    const payload =
      type == 2
        ? {
            receiver: read.string(),
            message: read.string(),
          }
        : {
            message: read.string(),
          };
      
      const parsedMsg = payload.message.replaceAll(/\x00/gi, "");

      console.log({op: "0x7025", message: payload.message});

   

    //       console.log({payload, parsedMsg})

    // if (parsedMsg.startsWith("!")) {
    //   const [command, ...args] = parsedMsg.split(" ");

    //   const chatCommand = new ChatCommands({
    //     sec_primary,
    //     sec_content,
    //     chatType: CHAT_TYPES[type],
    //     CharName16,
    //     params: args,
    //   });

    //   const result = await chatCommand.resolve(command.replace(/!/, ""));

    //   if (result) return result;
    // }

    // await dataService.post("/chat-logs", {
    //   UserJID,
    //   CharID,
    //   type,
    //   ...payload,
    //   message: parsedMsg,
    // });

    return [{packet}];
  } catch (e) {
    console.log(`ChatRequest err`, e);
    return [];
  }
}

export default ChatRequest;
