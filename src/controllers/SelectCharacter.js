import {playerNotice} from "@lib/utils/Packets";

async function SelectCharacter({config, stream: {reader}, logger, api: {data: dataService}, memory, info}, packet) {
  try {
    const read = new reader(packet.data);
    const name = read.string();

    const {
      data: {
        results: [
          {
            CharID,
            CharName16,
            characterLink: {
              user: {JID: UserJID, StrUserID, sec_primary, sec_content},
            },
          },
        ],
      },
    } = await dataService.post("/characters/search", {
      eager: {
        characterLink: {
          user: {},
        },
        $where: {
          CharName16: name,
        },
      },
    });

    const memory_init = {
      CharName16,
      CharID,
      UserJID,
      sec_primary,
      sec_content,
      StrUserID,
      lastActivityTime: new Date(),
      isAwayStatus: true,
    };

    // // remove old connections (if any):
    // await dataService.delete("/connections", {
    //   CharID,
    //   UserJID,
    // });

    // store current connection:
    await dataService.put("/connections", {
      where: {
        connection_id: info.id,
        context: config.module,
      },
      payload: {
        CharID,
        UserJID,
      },
    });

    Object.keys(memory_init).map(key => memory.set(key, memory_init[key]));

    logger.log("info", "join_game", memory_init);

    return [
      {packet}, // original packet to client
      {
        // packet for every other player online:
        ...playerNotice(`[${CharName16}] joined the game.`),
        target: "global",
      },
    ];
  } catch (e) {
    console.log(`SelectCharacter_ERR`, e);
    return [];
  }
}

export default SelectCharacter;
