async function FixPartyMatching({logger, config: {maxPartyMatchingPacketSize}, stream: {readed, writer}}, packet) {
  try {
     if (!packet.data.length || packet.data.length <= maxPartyMatchingPacketSize) {
      return [
        {
          packet,
        },
      ];
    } else {
      // logger.log(`error`, `attempted_to_hack`, {
      //     packet,
      // });
      return [
        {
          exit: true,
        },
      ];
    }
  } catch (e) {
    console.log(`PartyMatching_ERR`, e);
    return [];
  }
}

export default FixPartyMatching;
