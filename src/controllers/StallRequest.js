async function StallRequest(
    {
        logger,
        config: {
            CHAT_TYPES,
        },
        stream: {
            reader,
            writer,
        },
        memory,
        api: {
            account,
            shard,
            proxy,
        }
    },
    packet,
) {
    try {

        const read = new reader(packet.data);

        const name = read.string();
    
        const CharName16 = memory.get('CharName16');
        const UserJID = memory.get('UserJID')
    
        // const {
        //     data: [
        //         {
        //             LatestRegion,
        //             PosX,
        //             PosY,
        //             PosZ,
        //         }
        //     ]
        // } = await shard.get(`/_char`, {
        //     params: {
        //         sort: JSON.stringify(['CharID']),
        //         filter: JSON.stringify({
        //             CharName16,
        //         }),
        //     }
        // });
    
        // logger.log('info', 'StallRequest', {
        //     UserJID,
        //     CharName16,
        //     LatestRegion,
        //     PosX,
        //     PosY,
        //     PosZ,
        //     StallName: name,
        // });
    
        // // stall location verification here;
    
        
    
        return [
            { packet },
        ];
    } catch (e) {
        console.log(`[StallRequest]->error:`, e);
        return [];
    }
}

export default StallRequest;