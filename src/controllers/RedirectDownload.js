import redirects from '@config/redirects';

async function RedirectDownload(
    {
        stream: { 
            reader,
            writer,
        },
        logger
    },
    packet
) {
    const read = new reader(packet.data);
    const status = read.uint8();

    let _packet = {};

    if (status == 2) {
        const statusCode = read.uint8();
        if (statusCode == 2) {

            const host = read.string();
            const port = read.uint16();

            logger.log('info', 'update', {host, port})
            

            const redirect = redirects[`${host}:${port}`];

            const version = read.uint32();
            let hasEntry = read.uint8();

            const write = new writer();

            write.uint8(status);
            write.uint8(statusCode);

            write.string(redirect.host);
            write.uint16(redirect.port);

            write.uint32(version);
            write.uint8(hasEntry);

            while (hasEntry == 1) {

                const id = read.uint32();
                const name= read.string();
                const path = read.string();
                const size = read.uint32();
                const toPack = read.uint8();

                hasEntry = read.uint8();

                write.uint32(id);
                write.string(name);
                write.string(path);
                write.uint32(size);
                write.uint8(toPack);
                write.uint8(hasEntry);
            }

            _packet = {
                ...packet,
                data: write.toData()
            };
        }
    } else {
        _packet = packet;
    }

    return [{
        packet: _packet
    }];
}

export default RedirectDownload;
