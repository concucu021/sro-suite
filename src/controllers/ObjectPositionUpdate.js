async function ObjectPositionUpdate({logger, stream: {reader}, config}, packet) {
  try {
    const read = new reader(packet.data);

    const ObjecID = read.uint32();
    const clickType = read.uint8();
    const xSec = read.uint8();
    const ySec = read.uint8();

    const x = read.uint16();
    const y = read.uint16();
    const z = read.uint16();

    logger.log("info", "ObjectPositionUpdate", {ObjecID, clickType, xSec, ySec, x, y, z});

    return [
      {
        packet,
      },
    ];
  } catch (e) {
    console.log(`ObjectPositionUpdate_ERR`, e);
    return [];
  }
}

export default ObjectPositionUpdate;
