async function AutoCaptcha({stream, config}) {
  try {
    const {writer} = stream;

    const write = new writer();
    write.string(config.CAPTCHA);

    return [
      {
        packet: {
          opcode: 0x6323,
          data: write.toData(),
        },
        target: "remote",
      },
    ];
  } catch (e) {
    console.log(`AutoCaptcha_ERR, `, e);
    return [];
  }
}

export default AutoCaptcha;
