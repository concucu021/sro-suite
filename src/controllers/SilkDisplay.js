async function SilkDisplay({stream: {writer}, api: {data: DataService}, memory, logger}, packet) {
  try {
    const write = new writer();
    const JID = memory.get("UserJID");

    const {
      data: {
        results: [{silk_own, silk_gift, silk_point}],
      },
    } = await DataService.post(`/wallet/search`, {
      JID,
    });

    logger.log("info", "sent_silk_info", {
      silk_own,
      silk_gift,
      silk_point,
    });

    for (const amount of [silk_own, silk_gift, silk_point]) {
      write.uint32(amount);
    }

    return [
      {
        packet: {
          opcode: 0x3153,
          data: write.toData(),
        },
        target: "client",
      }, // silk in item mall
      // {
      //   packet,
      //   target: "remote",
      // }, // 
    ];
  } catch (e) {
    console.log(`[SilkDisplay]->error:`, e);
    return [];
  }
}

export default SilkDisplay;
