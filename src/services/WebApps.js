import config from "@config/WebMall";
import WebAppServer from "@root/src/lib/AppServer";
import webmall from "@root/src/lib/AppServer/routes/webmall";
import magiclamp from "@root/src/lib/AppServer/routes/magiclamp";
import survey from "@root/src/lib/AppServer/routes/survey";
import daily from "@root/src/lib/AppServer/routes/daily";

class WebApps extends WebAppServer {
  constructor() {
    super(config);

    // Web Mall:
    this.server.get("/webmall", webmall.index);
    this.server.get("/webmall/categories", webmall.api_categories);
    this.server.get("/webmall/session", webmall.api_query_update);
    this.server.get("/webmall/items", webmall.api_items);

    // Magic Lamp:
    this.server.get("/magiclamp", magiclamp.index);

    // Survey:
    this.server.get("/survey", survey.index);

    // Daily Window:
    this.server.get("/daily", daily.index);
  }
}

export default WebApps;
