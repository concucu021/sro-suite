import DataServer from "@lib/DataServer";
import config from "@config/API";

class Server extends DataServer {
  constructor() {
    super({
      port: config.data.port,
      host: "0.0.0.0",
      logger: config.data.development,
    });
  }
}

export default Server;
