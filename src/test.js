import net from "net";
import Network from "@lib/Security/Network";
import config from "@config/GatewayServer";

const {LOCAL, REMOTE, module} = config;
const network = new Network(config);

const clients = [];

const connection = async client => {
  const {remoteAddress, remotePort} = client;
  
  // UniqueID for direct Network access:
  const id = network.assignUniqueInstance(`${remoteAddress}:${remotePort}`);

  // TODO: validate whether the connection is eligible to connect
  network.addConnection(id);

  // Network model:
  // console.log({network});


  // Client -> Network
  client.on("data", buffer => {
    // TODO: do middlewares here
    // TODO: rate limitting

    // deliver to specific remote in network:
    network.send(id, buffer);
  });

  // Network -> Client
  network.on("data", ({id, buffer}) => {
    // TODO: do middlewares here

    // deliver to client:
    client.write(buffer);
  });

  // Exit cases:
  ["error", "close"].map(code => client.on(code, () => console.log({id, code})));
};

const server = net.createServer();

server.on("connection", connection);
server.on("listening", () => console.log({module, listen: LOCAL}));

server.listen(LOCAL.port, LOCAL.host);
