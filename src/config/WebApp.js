export default {
    //cgi.silkroad.pw/api/user_certify
    user_certify: {
        host:"0.0.0.0",
        port: 6400,
    }
    //cgi.silkroad.pw/store
    store: {
        host: "0.0.0.0",
        port: 6401
    },
    //cgi.silkroad.pw/magiclamp
    magic_lamp: {
        host: "0.0.0.0",
        port: 6402
    },
    //cgi.silkroad.pw/dailylogin
    daily_login: {
        host: "0.0.0.0",
        port: 6403
    },
}