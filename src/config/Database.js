import dotenv from "dotenv";

dotenv.config();

const {DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DIALECT, DB_INSTANCE_NAME} = process.env;

export default {
  host: DB_HOST || "127.0.0.1",
  username: DB_USERNAME || "sa",
  password: DB_PASSWORD || "",
  dialect: DB_DIALECT || "mssql",
  options: DB_INSTANCE_NAME
    ? {
        instanceName: DB_INSTANCE_NAME,
      }
    : {},
};
