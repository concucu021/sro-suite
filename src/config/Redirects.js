import dotenv from "dotenv";

dotenv.config();

const {NODE_ENV, REDIRECT_AGENT_IP, REDIRECT_AGENT_PORT, REDIRECT_DOWNLOAD_IP, REDIRECT_DOWNLOAD_PORT} = process.env;
const isDevelopment = NODE_ENV !== "production";

export default {
  //AgentServer1
  "138.201.58.79:15884": {
    host: REDIRECT_AGENT_IP || "127.0.0.1",
    port: REDIRECT_AGENT_PORT || 15884
  },
  // AgentServer2
  // "138.201.58.79:15885": {
  //   host: REDIRECT_AGENT_IP || "127.0.0.1",
  //   port: REDIRECT_AGENT_PORT || 15885
  // },
  // unlimited agents.. stick to pattern.

  // other module e.g. DownloadServer (must be hooked redirect packet in gw)
  // '148.251.195.215:15881': { // <-- IP FROM
  //     host: REDIRECT_DOWNLOAD_IP || '148.251.195.215', // <-- IP TO
  //     port: REDIRECT_DOWNLOAD_PORT || 15881, // <-- PORT TO
  // },
};
