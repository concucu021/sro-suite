import dotenv from "dotenv";

dotenv.config();

const {BIND_API_PORT, NODE_ENV} = process.env;

export default {
  data: {
    host: `http://127.0.0.1`,
    port: BIND_API_PORT || 8071,
    development: NODE_ENV && NODE_ENV === "development",
  },
};
