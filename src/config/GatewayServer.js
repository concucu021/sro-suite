import dotenv from "dotenv";

dotenv.config();

const {MODULE, IP_LIMIT, HWID_LIMIT, BIND_IP, BIND_PORT, REMOTE_IP, REMOTE_PORT} = process.env;

export default {
  module: MODULE || "GatewayServer",
  LIMITS: {
    IP: IP_LIMIT || 20,
    HWID: HWID_LIMIT || 3,
  },
  CAPTCHA: "0",
  LOCAL: {
    HOST: BIND_IP || "0.0.0.0", // ip client connects to
    PORT: BIND_PORT || 15779,
  },
  REMOTE: {
    HOST: REMOTE_IP || "138.201.58.79", // ip proxy connects to
    PORT: REMOTE_PORT || 15779,
  },
  middlewares: {
    client: {},
    remote: {
      0xa10a: "RedirectAgentServer",
      // 0x2116: "AuthCodeRequest", // not ready for use
    },
  },
  whitelist: [0x6104, 0x6100, 0x6107, 0x6101, 0x610a, 0x6117],
};
