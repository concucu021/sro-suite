import dotenv from "dotenv";

dotenv.config();

const {MODULE, BIND_IP, BIND_PORT, REMOTE_IP, REMOTE_PORT, IP_LIMIT, HWID_LIMIT, BLOCKED_COUNTRIES} = process.env;

export default {
  module: MODULE || "AgentServer",
  LOCAL: {
    HOST: BIND_IP || "0.0.0.0",
    PORT: BIND_PORT || 15884,
  },
  REMOTE: {
    HOST: REMOTE_IP || "138.201.58.79",
    PORT: REMOTE_PORT || 15884,
  },
  LIMITS: {
    IP: IP_LIMIT || 20,
    HWID: HWID_LIMIT || 3,
  },
  BANNED_COUNTRY_CODES: BLOCKED_COUNTRIES ? BLOCKED_COUNTRIES.split(",") : [],
  CHAT_TYPES: {
    1: "Public",
    2: "Private",
    3: "GM Public",
    4: "Party",
    5: "Guild",
    6: "Global",
    7: "GM Notice",
    9: "Stall",
    11: "Union",
    12: "Global",
    13: "NPC Quest",
    16: "Academy",
  },
  LIMIT_UNIQUES_PER_DAY: 7,
  UNIQUES: {   
    //(INT) UNIQUEID: {name,reward,cap},
    8972: {
      name: "Demon Shaitan",
      reward: 5,
      cap: 110,
    },
    9484: {
      name: "Roc",
      reward: 40,
      cap: 130,
    },
    31244: {
      name: "Merich",
      reward: 5,
      cap: 130,
    },
    31500: {
      name: "Khulood",
      reward: 5,
      cap: 130,
    },
    31756: {
      name: "Karkadan",
      reward: 5,
      cap: 130,
    },
    32012: {
      name: "Kidemonas",
      reward: 5,
      cap: 130,
    },
    41228: {
      name: "Captain Ivy",
      reward: 2,
      cap: 64,
    },
    41484: {
      name: "Tiger Girl",
      reward: 2,
      cap: 64,
    },
    48652: {
      name: "Uruchi",
      reward: 2,
      cap: 76,
    },
    53772: {
      name: "Isyutaru",
      reward: 3,
      cap: 85,
    },
    56844: {
      name: "Medusa",
      reward: 20,
      cap: 130,
    },
    57868: {
      name: "Lord Yarkan",
      reward: 4,
      cap: 101,
    },
    61196: {
      name: "Cerberus",
      reward: 2,
      cap: 76,
    },
  },
  weather: {
    type: [1, 1, 1, 1, 2, 1, 2], // 1 = normal, 2 = rain, 3 = snowing (the more times repeat in array the more chance having that weather)
    intensity: [10, 5, 25, 10], // random values in array (same rules as above)
  },
  goriExchangeGoldMinimum: 5000000,
  timeUntilAwayStatus: 60 * 5, // seconds
  maxPartyMatchingPacketSize: 200, //bytes - packet.data payload size
  middlewares: {
    client: {
      //OPCODE: MODULE
      0x7001: "SelectCharacter",
      0x7025: "ChatRequest",
    },
    remote: {
      //OPCODE: MODULE
      0x300c: "UniqueKilled",
      0x3809: "UpdateWeather",
      0x3026: "Debug",
    },
  },
  hooks: {
    exit: "onAgentDisconnect",
  },
  whitelist: [
    0x37b7, 0x37ee, 0x7554, 0x6118, 0x6106, 0x6101, 0x7534, 0x70ed, 0x7539, 0x7425, 0x7090, 0x758b, 0x7504, 0x7421, 0x6314, 0x7517, 0x6102, 0x7568, 0x7505, 0x7533, 0x7093, 0x7538, 0x7533, 0x7559, 0x1337, 0x7565, 0x7558, 0x3564, 0x7474, 0x7150, 0x7151, 0x7155, 0x716a, 0x7202, 0x7203, 0x70a1, 0x747b,
    0x747a, 0x7478, 0x347f, 0x747e, 0x747d, 0x7157, 0x70a2, 0x7470, 0x7471, 0x7473, 0x747c, 0x7475, 0x730c, 0x730a, 0x7302, 0x7046, 0x74d4, 0x70e3, 0x70b1, 0x70b2, 0x70b3, 0x70b5, 0x70b4, 0x70ba, 0x730d, 0x70c6, 0x7420, 0x70cb, 0x7117, 0x7304, 0x3303, 0x730b, 0x7308, 0x7477, 0x7309, 0x7116, 0x34a9,
    0x7005, 0x751a, 0x7168, 0x3091, 0x3053, 0x70c0, 0x7031, 0x70a7, 0x704f, 0x70c7, 0x7113, 0x703f, 0x715f, 0x7161, 0x7462, 0x7463, 0x7121, 0x7472, 0x34b6, 0x3019, 0x7024, 0x7023, 0x7160, 0x7402, 0x7050, 0x7051, 0x751c, 0x751d, 0xb070, 0x7006, 0x70d8, 0x70d9, 0x70db, 0x30d4, 0x7515, 0x7119, 0x7118,
    0x711a, 0x70ea, 0x705e, 0x705d, 0x74df, 0x74e0, 0x7516, 0x30c4, 0x7483, 0x34bf, 0x7519, 0x70e6, 0x705b, 0x70e4, 0x7007, 0x7450, 0x7001, 0xb04c, 0xb03e, 0x7034, 0xb034, 0x704c, 0x703e, 0x7158, 0x703c, 0x74d3, 0x74b2, 0x7509, 0x7506, 0x750a, 0x750b, 0x750c, 0x7507, 0x7508, 0x7025, 0x2113, 0x7074,
    0x704b, 0x2002, 0x9000, 0x6103, 0x3012, 0x3080, 0x7010, 0x7045, 0x750e, 0x70e5, 0x70f0, 0x70f2, 0x7062, 0x7061, 0x7063, 0x706b, 0x706c, 0x706d, 0x306e, 0x7069, 0x706a, 0x7060, 0x7081, 0x7083, 0x7084, 0xb082, 0x7021, 0x705a, 0x7059, 0x3014, 0x70e1, 0x70e2, 0x7082, 0x70f9, 0x70f4, 0x7259, 0x7104,
    0x7256, 0x70ff, 0x7258, 0x70f3, 0x70f6, 0x70fa, 0x7103, 0x7105, 0x7106, 0x7107, 0x70fb, 0x70fc, 0x70fd, 0x7110, 0x7112, 0x7114, 0x7501, 0x725a, 0x7250, 0x7251, 0x7252, 0x70f1, 0x7476, 0x70c5, 0x7557, 0x372c, 0x3785, 0x375f, 0x3730, 0x3700, 0x3537, 0x373f, 0x377c, 0x3745, 0x3703, 0x37A3, 
  ],
};
