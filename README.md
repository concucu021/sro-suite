# SRO SUITE
PROXY, WEBAPPS (Mall, Daily, Survey, Lamp)
- WIP: WORK IN PROGRESS
- For questions, please join following discord group: https://discord.gg/Gu5QUrVYyu 

### Prerequesities
- NodeJS (v16+): https://nodejs.org/
- Silkroad Online game client & server (2015 ISRO-R version)
- Yarn package manager: `npm i -g yarn`
- PM2 `yarn add -g pm2`

### Setup
- Install dependencies `yarn`
- Setup `src/config/AgentServer.js`, `src/config/GatewayServer.js` and `src/config/Redirects.js` accordingly.
- Rename `.env.example` to `.env` and insert your database config accordingly to the sample provide (note that it is best to use connection over ip instead of named instances)
- Run `yarn install:db` to create (or drop & create again) the initial database.

### Launch in development mode
- GatewayServer `yarn dev:GatewayServer`
- AgentServer `yarn dev:AgentServer`
- DataServer `yarn dev:DataServer`
- WebApps (WebMall, Magic Lamp, Survey, Daily) `yarn dev:WebApps`

### Launch in production mode
- GatewayServer `yarn GatewayServer`
- AgentServer `yarn AgentServer`
- DataServer `yarn DataServer`
- WebApps (WebMall, Magic Lamp, Survey, Daily) `yarn WebApps`
- All at once - install pm2 and refer to pm2 guide: `pm2 start && pm2 dashboard`

# DataServer API Docs:
- http://localhost:6440/documentation/index.html

Please see `.bat` files for more examples.

### Build
`yarn build`

## Building SilkroadSecurity
- Install Visual Studio or Build Tools 2022 with C++ desktop stack.
- Install boost 1_88_0 from https://www.boost.org/users/history/version_1_80_0.html
- Extract the package in your suitable directory and run `bootstrap.bat` from an elevated shell (you might need to set the `--msvs_version=2022` attribute)
- When above finished run `.\b2` from same path and you should be good to go.

You'll need to adjust `binding.gyp` in `silkroad-security` folder to similar as below (change path to your own boost installation):

```json
{
  "targets": [
    {
      "target_name": "SilkroadSecurityJS",
      "cflags!": [ "-fno-exceptions" ],
      "cflags_cc!": [ "-fno-exceptions" ],
      "sources": [
        "./src/SilkroadSecurity/blowfish.cpp",
        "./src/SilkroadSecurity/silkroad_security.cpp",
        "./src/SilkroadSecurity/stream_utility.cpp",

        "./src/SilkroadSecurityJS.cpp",
        "./src/main.cpp"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "C:/dev/boost_1_80_0"
      ],
      "libraries": [
        "C:/dev/boost_1_80_0/stage/lib/libboost_random-vc143-mt-s-x64-1_80.lib"
      ],
      "defines": [ "NAPI_DISABLE_CPP_EXCEPTIONS" ],
    }
  ]
}
```

## What else?
- packet docs https://github.com/DummkopfOfHachtenduden/SilkroadDoc
- silkroad-security https://github.com/EmirAzaiez/SilkroadSecurityJS
