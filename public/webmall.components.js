var DebugCodeComponent = function (code) {
  var frame = $("<div />").addClass("code");
  frame.html(JSON.stringify(code, null, 2));
  return frame;
};

var renderSubCategoryItems = function (container, cf) {
  $(container).append($("<div class='alert'><img src='/images/loader.gif' /></div>"));

  $.get("/api/store/items?shop_no=" + cf.ref_no + "&shop_no_sub=" + cf.sub_no, function (data) {
    $(container).empty();
    var itemWrapper = $("<div />").addClass("items");

    if (data.results.length > 0) {
      $(data.results).each(function (i, item) {
        // console.log(item);

        var itemContainer = $("<div />").addClass("item");

        var item_name = item.detail ? item.detail.item_name_eng : "n/a";

        var imageTitleContainer = $("<div />").addClass("title-container");
        var priceContainer = $("<div />").addClass("price-container");
        var buttonsContainer = $("<div />").addClass("buttons-container");

        var image = $("<img />", {
          id: item.package_code,
          src: "/images/items/" + item.package_code + ".jpg",
          alt: item_name,
        });

        var title = $("<div />").addClass("title").html(item_name);
        var price = $("<div />")
          .addClass("price")
          .html(item.silk_price + " silk");

        var buyButton = $("<button class='purchase'>Purchase</button>");
        var cartButton = $("<button class='addToCart'></button>");

        buyButton.on("click", function () {
          onClickPurchaseItem(item);
        });

        cartButton.on("click", function () {
          onClickCartItem(item);
        });

        imageTitleContainer.append(image);
        imageTitleContainer.append(title);

        var tooltipContent = "<h3>" + item.lang["package_name"] + "</h3>";
        tooltipContent += "<b>Description:</b><br />";
        tooltipContent += "<span>" + item.lang[token.loc + "_explain"] + "</span><br />";
        tooltipContent += "<b>Item Info:</b><br />";
        tooltipContent += "<span>" + item.lang[token.loc + "_use_method"] + "</span><br />";
        tooltipContent += "<b>Restrictions:</b><br />";
        tooltipContent += "<span>" + item.lang[token.loc + "_use_restriction"] + "</span><br />";

        imageTitleContainer.attr("tiptext", "HELLOW WORLD").attr("title", tooltipContent);

        imageTitleContainer.hover(function (event) {
          // console.log(item);
          if (event.type === "mouseleave") {
            $(this).attr("title", $(this).data("tiptext"));
            $(".tooltip").remove();
          } else {
            var titleText = $(this).attr("title");
            $(this).data("tiptext", titleText).removeAttr("title");
            $('<p class="tooltip"></p>')
              .html(titleText)
              .appendTo("body")
              .css("top", event.pageY - 10 + "px")
              .css("left", event.pageX + 20 + "px")
              .fadeIn();
          }
        });

        imageTitleContainer.mousemove(function (event) {
          $(".tooltip")
            .css("top", event.pageY - 10 + "px")
            .css("left", event.pageX + 20 + "px");
        });

        priceContainer.append(price);
        buttonsContainer.append(buyButton);
        buttonsContainer.append(cartButton);

        itemContainer.append(imageTitleContainer);
        itemContainer.append(priceContainer);
        itemContainer.append(buttonsContainer);

        itemWrapper.append(itemContainer);
      });

      $(container).append(itemWrapper).fadeIn("slow");
    } else {
      $(container).append($("<div class='alert'>Nothing here yet...</div>")).fadeIn("slow");
    }
  });
};

var renderSubCategories = function (subCategories) {
  $(".categories").remove();

  var scrollFrame = $("<div />").addClass("categories");

  var subFrame = $("<div />").addClass("categories-wrap");
  $(subCategories).each(function (i, cf) {
    var sc_container = $("<div />").addClass("category");

    var sc_header = $("<div />")
      .html(cf["sub_name_" + token.loc])
      .addClass("header");

    var sc_body = $("<div />").addClass("body"); //.html(cf.ref_no + ":" + cf.sub_no);

    renderSubCategoryItems(sc_body, cf);

    sc_container.append(sc_header);
    subFrame.append(sc_container);
    subFrame.append(sc_body);
  });

  scrollFrame.append(subFrame);

  $("div[data-view='suggested']").append(scrollFrame);
};

var renderCategoryTabs = function (categories) {
  var selectedFrame = 0;
  var initialCategory = categories[selectedFrame];
  var subCategories = initialCategory.subCategories;

  var frame = $("<div />").addClass("tabs");

  $(categories).each(function (i, c) {
    var cf = $("<div />")
      .html(c["shop_name_" + token.loc])
      .addClass("tab");

    cf.on("click", function () {
      $(".tab").each(function () {
        $(this).removeClass("active");
      });
      cf.addClass("active");
      renderSubCategories(c.subCategories);
    });

    if (i === 0) {
      cf.addClass("active");
    }

    frame.append(cf);
  });

  $("div[data-view='suggested']").append(frame);

  renderSubCategories(initialCategory.subCategories);
};

var renderBalance = function () {
  var silk_types = [
    {
      name: "Premium Silk",
      value: "silk_own_premium",
      icon: "silk_premium",
    },
    {
      name: "Points",
      value: "silk_point",
      icon: "silk_premium",
    },
    {
      name: "Gift Silk",
      value: "silk_gift",
      icon: "silk_premium",
    },
    {
      name: "Premium Gift",
      value: "silk_gift_premium",
      icon: "silk_premium",
    },
    {
      name: "Silk",
      value: "silk_own",
      icon: "silk",
    },
  ];

  var silk_update_times = 0;

  var createSilkTemplate = function (name, amount, icon) {
    var template = '<div class="values"><div class="label">' + name + ":</div>";
    template += '<div class="value"><img src="/images/item_img/' + icon + '.gif" /> ' + amount + "</div>";
    template += "</div>";
    return template;
  };

  if (token) {
    $.get("/api/store/session?jid=" + token.jid + "&key=" + token.key, function (supdate) {
      if (supdate) {
        silk_update_times++;
        var tpl = "";

        $(silk_types).each(function (i, item) {
          tpl += createSilkTemplate(item.name, supdate.wallet[item.value], item.icon);
        });

        $("#wallet").empty();
        $("#wallet").append($(tpl));
      }
    });
  }
};
