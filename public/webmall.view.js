var VIEW_SUGGESTED = function () {
  $.get("/api/store/categories", function (data) {
    // render categories component:
    renderCategoryTabs(data.results, token.loc);
  });
};

var VIEW_ALL = function () {};

var VIEW_RESERVED = function () {};

var VIEW_HISTORY = function () {};

var view_indexes = {
  suggested: VIEW_SUGGESTED,
  all: VIEW_ALL,
  reserved: VIEW_RESERVED,
  history: VIEW_HISTORY,
};
