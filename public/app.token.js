var getSession = function () {
  var token = $("#session").attr("token");
  var decoded = Base64.decode(token);
  var split = decoded.split("|");

  return {
    jid: split[0],
    key: split[1],
    loc: split[2],
  };
};
