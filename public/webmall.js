var currentPage = "suggested";
var token = getSession();

$.ajaxSetup({
  cache: false
});

var onClickRefresh = function () {
  window.location.reload();
};

var triggerView = function () {
  $("div[data-view]").each(function (view) {
    var instance = $(this);
    var identifier = instance.attr("data-view");

    // cleanup:
    instance.empty();

    if (identifier === currentPage && view_indexes[identifier]) {
      // launch the controller of that page:
      view_indexes[identifier]();

      instance.attr("style", "display:block");
    } else {
      instance.attr("style", "display:none");
    }
  });
};

var onClickCategory = function () {
  var instance = $(this);
  var selectedPage = instance.attr("data-link");

  currentPage = selectedPage;

  $("div[data-link]").each(function (button) {
    var _instance = $(this);
    var _identifier = _instance.attr("data-link");

    if (_identifier === currentPage) {
      _instance.addClass("active");
    } else {
      _instance.removeClass("active");
    }
  });

  triggerView();
};

var showConfirmation = function() {
  
}

var onClickPurchaseItem = function(item) {
  // console.log("purchase", item);
};

var onClickCartItem = function(item) {
  // console.log("cart", item);
};

// TODO FIX SHRINKING UI:
var sessionUpdate = function() {
  return setTimeout(function() {
    renderBalance();
    sessionUpdate();
  }, 2000);
};

$("div[data-link]").on("click", onClickCategory);
$("#refresh").on("click", onClickRefresh);

triggerView();


renderBalance();
