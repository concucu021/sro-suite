let stream = require("./stream");
let SSA = require("../build/Release/Security.node").Security;

class Security extends SSA {
  constructor(identity_name = "SR_Client", identity_flag = 0) {
    super(identity_name, identity_flag);
  }

  generateHandshake(blowfish = true, security_bytes = true, handshake = true) {
    return super.GenerateHandshake(blowfish, security_bytes, handshake);
  }

  changeIdentity(identity_name, identity_flag) {
    return super.ChangeIdentity(identity_name, identity_flag);
  }

  receive(data = []) {
    if (data instanceof Array) {
      return super.Recv(data);
    } else if (data instanceof Buffer) {
      return super.Recv(data.toJSON().data);
    }
  }

  processIncomingStream() {
    return super.GetPacketToRecv() || [];
  }

  processOutgoingStream() {
    return super.GetPacketToSend() || [];
  }

  send(opcode = 0x0000, data = [], encrypted = false, massive = false) {
    let ndata = data;

    if (data instanceof Buffer) {
      ndata = data.toJSON().data;
    } else if (data instanceof stream.stream) {
      ndata = data.toData();
    }

    return super.Send(opcode, ndata, encrypted, massive);
  }
}

module.exports = {
  Security,
  stream: {
    reader: stream.reader,
    writer: stream.writer,
  },
};
