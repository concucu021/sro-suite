{
    "targets": [
        {
            "target_name": "Security",
            "cflags!": ["-fno-exceptions"],
            "cflags_cc!": ["-fno-exceptions"],
            "sources": [
                "./src/SilkroadSecurity/blowfish.cpp",
                "./src/SilkroadSecurity/silkroad_security.cpp",
                "./src/SilkroadSecurity/stream_utility.cpp",
                "./src/Security.cpp",
                "./src/main.cpp"
            ],
            "include_dirs": [
                "<!@(node -p \"require('node-addon-api').include\")",
                "C:/dev/boost_1_80_0"
            ],
            "libraries": [
                "C:/dev/boost_1_80_0/stage/lib/libboost_random-vc143-mt-s-x64-1_80.lib"
            ],
            "define": ["NAPI_DISABLE_CPP_EXCEPTIONS"],
        }
    ]
}
