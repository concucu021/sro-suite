module.exports = {
    up: ({schema, fn}) =>
      schema.createTable("connections", t => {
        t.increments("id");
        
        t.enum("context", ["AgentServer", "GatewayServer"]);

        t.string("connection_id").notNullable();
        t.string("remote").notNullable();
        t.string("port").notNullable();

        t.integer("UserJID");
        t.integer("CharID");

        t.timestamp("created").defaultTo(fn.now());
        t.timestamp("updated").defaultTo(fn.now());
      }),
    down: ({schema}) => schema.dropTable("connections"),
  };
  